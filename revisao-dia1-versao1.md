# Revisão

> **Orientação para quem está guiando:**
Revisar com as participantes os conceitos que foram vistos no dia anterior. Quem está guiando pode relembrar alguns termos e também pode perguntar para as meninas sobre os conceitos. Algumas ideias sobre o que revisar:
* Criamos programas para resolver problemas.
* Linguagens de programação são uma maneira de passarmos instruções aos computadores quando estamos desenvolvendo programas.
* Existem muitas linguagens de programação diferentes - e Scratch é considerada uma delas.
* No Scratch, cada bloco é um comando diferente.
