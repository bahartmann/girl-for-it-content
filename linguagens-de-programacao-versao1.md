# Resolvendo Problemas com Linguagens de Programação

Quando o primeiro computador foi criado, ele servia para para computar
trajetórias táticas, no contexto da II Guerra Mundial. Ao longo do tempo, novas
necessidades surgiram e a tecnologia dos computadores foi evoluindo. Hoje em
dia, usamos o computador para muitas outras finalidades, como editar textos ou
fazer pesquisas na internet. O Microsoft Word e o buscador do Google são exemplos
de **programas** que nos permitem realizar as tarefas desejadas. Assim como
esses, existem muitos outros programas, tanto localizados diretamente no nosso
computador quanto disponíveis na internet.

Cada um desses programas teve que ser desenvolvido por seres humanos, pois o
computador sozinho não é capaz de saber quais são as nossas necessidades. Quando
alguém está desenvolvendo um programa, ou seja, programando, usa uma **linguagem
de programação**. Linguagem de programação é uma ferramenta usada para humanos
se comunicarem com o computador. A maior parte é em forma de código, que deve
ser escrito de acordo com algumas regras, e serve para passarmos instruções
diversas ao computador.

Existem linguagens de programação mais antigas e mais recentes. Algumas são mais
legíveis e outras mais difíceis de se entender. Há as mais e as menos rápidas.
Veja abaixo como são os códigos de algumas linguagens:

> **Orientação para quem está guiando:**
Explicar o que cada parte dos códigos abaixo faz, incluindo: variáveis, operação
matemática e imprimir. Não é necessário explicar sobre o que "stdio.h" ou
"return 0" significam exatamente, mas é importante dizer que cada linguagem de
programação tem suas regras específicas e algumas exigem especificidades a mais
do que outras.

* **Linguagem C**

```C
#include <stdio.h>
int main()
{
    int primeiroNumero = 2;
    int segundoNumero = 3;

    int soma = primeiroNumero + segundoNumero;

    printf("%d + %d = %d", primeiroNumero, segundoNumero, soma);

    return 0;
}
```

* **Linguagem Python**

```python
primeiroNumero = 2
segundoNumero = 3

soma = primeiroNumero + segundoNumero

print(primeiroNumero, " + ", segundoNumero, " = ", soma)
```

Ambos os trechos de código vão imprimir `2 + 3 = 5`. Mas você consegue ver
algumas diferenças entre a escrita das duas linguagens de programação?

Quando estamos programando, temos que pensar em uma sequência de atividades a serem feitas para chegar ao objetivo. Por exemplo, se queremos fazer um programa para descobrir se uma pessoa é maior de idade, devemos pensar em cada passo para chegar até o final. Nesse caso, os passos seriam:

**1 →** Perguntar a idade da pessoa

**2 →** Guardar o número que a pessoa fornecer

**3 →** Verificar se o número é maior ou igual a 18

**4 →** Se o número for maior ou igual a 18, imprimir na tela "Você é maior de
idade!"

**5 →** Se o número for menor que 18, imprimir na tela "Você é menor de idade!"


Acima, vimos alguns problemas simples que podem ser resolvidos com programação.
Mas quando aprendemos a programar descobrimos que as possibilidades são muitas,
desde automatizar tarefas simples do dia a dia até criar produtos que podem ser
a próxima revolução mundial. Afinal, programação é a arte de controlar
computadores e fazer coisas incríveis com eles!

> **Orientação para quem está guiando:**
Antes de ir para a próxima etapa, é importante dizer que a expectativa não é que
elas saiam do evento programando de forma avançada. Deixar claro que é
improvável alguém saber tudo sobre programação apenas em dois dias. A ideia é
que ela entendam o básico e saibam alguns caminhos pelos quais seguir depois.
