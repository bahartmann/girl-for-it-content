# Mulheres na TI

Atualmente, o mercado de Tecnologia da Informação (TI) é predominantemente formado por homens. Mas não foi sempre assim. Nos primórdios da computação, houve grande participação das mulheres. As primeiras pessoas programadoras foram mulheres, ou seja, elas que faziam os computadores operarem. Veja abaixo algumas mulheres que tiveram grande importância no desenvolvimento da TI:

* Ada Lovelace - Matemática e escritora inglesa, escreveu o primeiro algoritmo a ser processado por uma máquina. É considerada a primeira programadora de toda a história.

* Hedy Lamarr - Atriz e engenheira de telecomunicações, inventou o precursor da comunicação Wireless durante a Segunda Guerra Mundial.

* Betty Snyder Holberton, Frances Bilas Spence, Jean Bartik, Kay McNulty Mauchly Antonelli, Marilyn Wescoﬀ Meltzer e Ruth Lichterman - Foram as responsáveis por programar e configurar o primeiro computador digital eletrônico de grande escala, o ENIAC.

![Mulheres com peças de computadores](https://i.imgur.com/4JzweyFl.jpg)

* Grace Hopper - Pioneira da computação, foi analista de sistemas da marinha e criadora da linguagem Flow-Matic, base para a criação da linguagem COBOL, utilizada até hoje.

* Evelyn Berezin - Era funcionária da Underwood Company quando criou o primeiro computador a ser usado em escritórios.

Na época em que as mulheres eram a maioria na programação, essa tarefa era vista como menos importante, enquanto a construção de hardwares (a parte "física" dos computadores) era um trabalho mais valorizado por supostamente ser mais difícil e, portanto, deveria ser realizado pelos homens.

À medida que a TI foi evoluindo e ganhando relevância, o interesse masculino pela programação aumentou. Aos poucos, o ambiente foi se masculinizando e, consequentemente, tornando-se mais hostil para as mulheres. Veja abaixo um gráfico que mostra a mudança da presença de mulheres na Ciência da Computação.

![Gráfico - O que aconteceu com as mulheres na Ciência da Computação?](https://i.imgur.com/74zbc71h.png)

Um fator que intensificou esse fenômeno foi a associação que a mídia fazia entre tópicos relacionados à tecnologia e os homens. Entre os anos 1980 e 1990, quando computadores pessoais e vídeo games começaram a ser comercializados, as famílias compravam para os meninos porque era o que aparecia nas propagandas e anúncios. Já estava culturalmente impregnado à distinção de gênero.

A diferença entre o número de homens e mulheres na computação continua ainda hoje. Segundo o PNAD de 2009, apenas 20% das pessoas que trabalham com Tecnologia da Informação no Brasil são mulheres. Isso se deve ao estereótipo da pessoa que trabalha com TI, falta de representatividade das mulheres e a uma cultura hostil com as mulheres do setor de TI. Porém, muita gente está agindo para mudar essa realidade. Há diversos eventos, grupos e outras iniciativas para incentivar que meninas e mulheres aprendam a programar e voltem a fazer parte da área de computação.

## Por que a área de TI pode ser interessante?

Segundo a Associação Brasileira de Empresas de Tecnologia da Informação e Comunicação, em 2016 havia 50 mil postos de trabalho esperando por pessoas qualificadas na área. Com isso, os salários também aumentam. Cada vez mais, atividades e serviços do dia a dia giram em torno de programas de computadores - desde lojas que vendem produtos online até empresas de transporte, como a Uber. Quem trabalha na área tem desafios bem interessantes e pode ter um impacto na vida de muitas pessoas.

Para quem quer entrar para o mercado de IT, é importante ser autodidata e ter dedicação. Proatividade e boa comunicação também são características atualmente muito necessárias para profissionais da área.

Vamos aprender mais sobre a atividade central da computação: a programação de computadores!
