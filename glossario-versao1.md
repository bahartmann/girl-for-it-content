Guia de termos

*alternative information* - informação alternativa
*anchor* - âncora
*big* - grande
*blue* - azul
*body* - corpo
*border radius* - raio da borda
*border-bottom* - borda inferior
*Cascading Style Sheets* - Folhas de estilo em cascata
*color* - cor
*dotted* - pontilhado
*emphasized* - enfatizado
*filter* - filtro
*font-family* - família de fontes
*head* - cabeçalho
*heading* - linha de título
*hello* - olá
*hover* - pairar
*HyperText Markup Language* - Linguagem de Marcação de Hipertexto
*Hypertext Reference* - referência de hipertexto
*line break* - quebra de linha
*line-height* - espessura de linha
*line-through* - linha atravessada
*list item* - item da lista
*list-style* - estilo de lista
*margin-right* - margem direita
*margin* - margem
*none* - nenhum
*opacity* - opacidade
*ordered list* - lista ordenada
*overline* - sobre-linha
*padding* - preenchimento
*script* - roteiro
*small* - pequeno
*solid* - sólido
*source* - origem
*strong* - forte
*tag* - etiqueta; rótulo
*text-decoration* - decoração do texto
*title* - título
*transition* - transição
*underline* - sublinhado
*unordered list* - lista desordenada
*vertical-align* - alinhamento vertical
*width* - largura
