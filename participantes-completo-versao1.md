# Girl for IT

# Agenda

| Dia 1                                           |          |
|-------------------------------------------------|:--------:|
| Check-in + Apresentação do evento + Boas vindas | 30 min   |
| Histórico das Mulheres na TI                    | 15 min   |
| Quebra-gelo                                     | 30 min   |
| Coffee Break                                    | 15 min   |
| Linguagens de Programação                       | 30 min   |
| Scratch e Games                                 | 2 horas  |
| Encerramento                                    | 10 min   |
| **Duração Total**                               | **4h10** |

| Dia 2                                           |          |
|-------------------------------------------------|:--------:|
| Revisão conteúdo anterior                       | 10 min   |
| HTML                                            | 45 min   |
| CSS                                             | 45 min   |
| Coffee Break                                    | 10 min   |
| Criar Blog                                      | 2 horas  |
| Painel com Mulheres na TI                       | 40 min   |
| Encerramento                                    | 15 min   |
| **Duração Total**                               | **4h45** |


# Mulheres na TI

Atualmente, o mercado de Tecnologia da Informação (TI) é predominantemente formado por homens. Mas não foi sempre assim. Nos primórdios da computação, houve grande participação das mulheres. As primeiras pessoas programadoras foram mulheres, ou seja, elas que faziam os computadores operarem. Veja abaixo algumas mulheres que tiveram grande importância no desenvolvimento da TI:

* Ada Lovelace - Matemática e escritora inglesa, escreveu o primeiro algoritmo a ser processado por uma máquina. É considerada a primeira programadora de toda a história.

* Hedy Lamarr - Atriz e engenheira de telecomunicações, inventou o precursor da comunicação Wireless durante a Segunda Guerra Mundial.

* Betty Snyder Holberton, Frances Bilas Spence, Jean Bartik, Kay McNulty Mauchly Antonelli, Marilyn Wescoﬀ Meltzer e Ruth Lichterman - Foram as responsáveis por programar e configurar o primeiro computador digital eletrônico de grande escala, o ENIAC.

![Mulheres com peças de computadores](https://i.imgur.com/4JzweyFl.jpg)

* Grace Hopper - Pioneira da computação, foi analista de sistemas da marinha e criadora da linguagem Flow-Matic, base para a criação da linguagem COBOL, utilizada até hoje.

* Evelyn Berezin - Era funcionária da Underwood Company quando criou o primeiro computador a ser usado em escritórios.

Na época em que as mulheres eram a maioria na programação, essa tarefa era vista como menos importante, enquanto a construção de hardwares (a parte "física" dos computadores) era um trabalho mais valorizado por supostamente ser mais difícil e, portanto, deveria ser realizado pelos homens.

À medida que a TI foi evoluindo e ganhando relevância, o interesse masculino pela programação aumentou. Aos poucos, o ambiente foi se masculinizando e, consequentemente, tornando-se mais hostil para as mulheres. Veja abaixo um gráfico que mostra a mudança da presença de mulheres na Ciência da Computação.

![Gráfico - O que aconteceu com as mulheres na Ciência da Computação?](https://i.imgur.com/74zbc71h.png)

Um fator que intensificou esse fenômeno foi a associação que a mídia fazia entre tópicos relacionados à tecnologia e os homens. Entre os anos 1980 e 1990, quando computadores pessoais e vídeo games começaram a ser comercializados, as famílias compravam para os meninos porque era o que aparecia nas propagandas e anúncios. Já estava culturalmente impregnado à distinção de gênero.

A diferença entre o número de homens e mulheres na computação continua ainda hoje. Segundo o PNAD de 2009, apenas 20% das pessoas que trabalham com Tecnologia da Informação no Brasil são mulheres. Isso se deve ao estereótipo da pessoa que trabalha com TI, falta de representatividade das mulheres e a uma cultura hostil com as mulheres do setor de TI. Porém, muita gente está agindo para mudar essa realidade. Há diversos eventos, grupos e outras iniciativas para incentivar que meninas e mulheres aprendam a programar e voltem a fazer parte da área de computação.

## Por que a área de TI pode ser interessante?

Segundo a Associação Brasileira de Empresas de Tecnologia da Informação e Comunicação, em 2016 havia 50 mil postos de trabalho esperando por pessoas qualificadas na área. Com isso, os salários também aumentam. Cada vez mais, atividades e serviços do dia a dia giram em torno de programas de computadores - desde lojas que vendem produtos online até empresas de transporte, como a Uber. Quem trabalha na área tem desafios bem interessantes e pode ter um impacto na vida de muitas pessoas.

Para quem quer entrar para o mercado de IT, é importante ser autodidata e ter dedicação. Proatividade e boa comunicação também são características atualmente muito necessárias para profissionais da área.

Vamos aprender mais sobre a atividade central da computação: a programação de computadores!


# Resolvendo Problemas com Programas de Computadores

Quando os primeiros computadores eletrônicos foram criados, eles tinham objetivos relativamente simples, como o cálculo de tabelas de tiro/escala de artilharia. Ao longo do tempo, novas necessidades surgiram e a tecnologia dos computadores evoluiu.

Atualmente, usamos o computador para muitas outras finalidades, como editar textos ou fazer pesquisas na internet. O Microsoft Word e o buscador do Google são exemplos de programas que nos permitem realizar algumas das tarefas que desejamos. Assim como esses, existem muitos outros programas, tanto localizados diretamente no nosso computador quanto disponíveis na internet.

Cada um desses programas teve que ser criado por seres humanos, pois o computador sozinho não é capaz de saber quais são as nossas necessidades. Chamamos a criação e desenvolvimento de programas de **programação**.

## Algoritmos

Quando estamos criando um programa para o computador, ou seja, programando, temos que pensar em uma sequência de atividades a serem feitas para chegar ao objetivo. Chamamos essas sequências de atividades de **algoritmos**. Um algoritmo é um conjunto de várias instruções lógicas ordenadas que atingem uma meta específica, assim como uma receita. Em um algoritmo, cada passo tem uma definição bem clara e uma ordem correta a ser realizado.

No nosso dia-a-dia, encontramos vários algoritmos em atividades cotidianas. O algoritmo para fazer um sanduíche de presunto, por exemplo, seria o seguinte:

**1 →** Pegar 2 fatias de pão

**2 →** Pegar maionese

**3 →** Passar maionese nas fatias de pão

**4 →** Pegar presunto

**5 →** Colocar uma fatia de presunto sobre uma fatia de pão

**6 →** Pegar a segunda fatia de pão e virar o lado com maionese para baixo

**7 →** Colocar segunda fatia de pão sobre o presunto

Observe que cada passo é importante para fazer o sanduíche, assim como a ordem. Se o passo 7 fosse executado antes do 6, teríamos um sanduíche com a maionese no lado de fora!

Também podemos encontrar algoritmos quando estamos resolvendo algum cálculo. Por exemplo, se queremos descobrir a média das notas que tiramos em 3 provas de uma matéria, passamos por vários passos até chegar a uma conclusão. Neste caso, os passos seriam:

**1 →** Anotar as notas de cada uma das 3 provas

**2 →** Somar as 3 notas

**3 →** Anotar a soma das notas

**4 →** Dividir a soma das notas por 3

Você consegue imaginar outras situações no nosso dia-a-dia que também são como algoritmos?

## Linguagem de programação

Já aprendemos que quando vamos programar, devemos pensar nas instruções lógicas, também chamadas de algoritmos. Mas como comunicamos ao computador quais são essas instruções? Como fazer o computador entender quais são os passos lógicos de um programa? Para isso, usamos **linguagens de programação**.

Linguagem de programação é uma ferramenta usada para humanos escreverem passos lógicos que o computador vai entender. A maior parte é em forma de código, que deve ser escrito de acordo com algumas regras, e serve para passarmos instruções diversas ao computador. Existem linguagens de programação mais antigas e mais recentes. Algumas são mais legíveis e outras mais difíceis de se entender. Veja abaixo como são os códigos de algumas linguagens:

* Linguagem C

```C
#include <stdio.h>
int main()
{
    int primeiroNumero = 2;
    int segundoNumero = 3;

    int soma = primeiroNumero + segundoNumero;

    printf("%d + %d = %d", primeiroNumero, segundoNumero, soma);

    return 0;
}
```

* Linguagem Python

```python
primeiroNumero = 2
segundoNumero = 3

soma = primeiroNumero + segundoNumero

print(primeiroNumero, " + ", segundoNumero, " = ", soma)
```

Ambos os trechos de código vão imprimir 2 + 3 = 5. Mas você consegue ver algumas diferenças entre a escrita das duas linguagens de programação?

Acima, vimos alguns problemas simples que podem ser resolvidos com programação. Mas quando aprendemos a programar descobrimos que as possibilidades são muitas, desde automatizar tarefas simples do dia a dia até criar produtos que podem ser a próxima revolução mundial. Afinal, programação é a arte de controlar computadores e fazer coisas incríveis com eles!

# Construindo um jogo com Scratch

Esse é um tutorial para construir um jogo enquanto aprendemos a programar! Para programar esse jogo, vamos usar o Scratch. Scratch é uma linguagem de programação gráfica, o que significa que ao invés de usar código somente em texto (como a maioria das linguagens de programação), ela é visual. Os comandos que vamos passar para o computador são blocos coloridos que podemos arrastar e organizar como desejamos, montando nosso programa.

## Cadastro no site do Scratch

Agora, vamos fazer um cadastro no *site* do Scratch para que possamos salvar os projetos feitos. Seguiremos os seguintes passos:

**1 →** Acesse o *site* https://scratch.mit.edu/.

**2 →** Clique em "Inscreva-se".

**3 →** Escolha um nome (sem espaços) e uma senha e preencha. Use uma senha que será lembrada! Clique em "Próximo".

**4 →** Preencha com o mês e ano de nascimento, gênero e país. Clique em "Próximo".

**5 →** Preencha duas vezes seu e-mail. Se você não tiver um e-mail, preencha com *sememail@bol.com*. Depois você poderá criar um e-mail e alterar seu cadastro. Clique em "Próximo".

**6 →** Pronto! Agora é só clicar em "Ok, vamos lá!".

Clique em "Explorar" na barra de cima e veja os projetos que tem por lá!

## Criando o projeto

Vamos aprender diversos conceitos de programação construindo um jogo!

No jogo que vamos construir, vamos ter uma personagem principal que tem como objetivo pegar um doce. Porém, no meio do caminho até o doce, há um morcego e um vilão que vão atrapalhar sua jornada.

**Curiosidade:** Quando estamos programando, chamamos a pessoa que vai usar o programa (ou seja, quem vai jogar o jogo) de usuária. Hoje, você não é a usuária, mas sim, a programadora do jogo!

Para começar, acesse o *site* https://scratch.mit.edu/projects/171181948/#editor. Esse é um projeto do Scratch. A parte da esquerda é onde vamos ver nosso jogo em ação. É possível ver que alguns objetos já foram adicionados nessa parte. Cada um desses objetos é chamado de **ator**. Depois, vamos poder adicionar diversos comportamentos à esses atores.

A parte da direita, é onde vamos adicionar comandos para o nosso jogo.

### Criando a personagem principal

Já temos alguns atores no jogo, mas precisamos criar a personagem principal. Para isso, siga os passos abaixo:

**1 →** Clique no bonequinho ao lado do botão "Novo ator".

**2 →** Escolha uma das imagens para ser sua personagem. Você pode rolar a página para ver mais opções. Clique na imagem escolhida e depois no botão "OK".

Agora a personagem está no jogo! Mas como você pode ver, ela está muito maior que os outros atores. Para corrigir isso e posicionar a personagem, siga os próximos passos:

**3 →** Clique no botão "Reduzir", que está na barra bem no alto da tela, representado por flechas apontando para dentro.

**4 →** Clique na sua personagem até que fique em um tamanho parecido com os outros atores. Quando estiver em um tamanho que você goste, clique em qualquer lugar do fundo lilás.

**5 →** Você pode mover os atores pela tela arrastando eles. Vamos colocar a personagem no lado esquerdo do jogo. Coloque o ponteiro do mouse em cima da sua personagem, segure o botão do mouse e o mova enquanto continua segurando o botão. Largue o botão quando a personagem estiver onde você quer.

![Figura 1](https://i.imgur.com/yLHbmQa.png)
*Figura 1*


### Mexendo com comandos - os blocos

No meio da tela, estão os comandos que podem ser dados aos atores. No Scratch, esses comandos são chamados de **blocos**. Clique no bloco "Mova 10 passos" e observe o que acontece com sua personagem quando você faz isso.

Há muitos tipos de blocos. Na parte de cima dos blocos, você pode mudar o tipo dos blocos que estão sendo mostrados.

**1 →** Clique em "Aparência" para mostrar os blocos que mudam a aparência dos objetos.

**2 →** Clique no bloco "diga *Hello!*¹ por 2 segundos".

¹ *hello* - olá

**3 →** Observe o que acontece quando você clica em "pense Hmm... por 2 segundos".

**4 →** Clique também no bloco "mostre" e depois no bloco "mostre" e acompanhe o que cada um faz.

Agora vamos fazer a personagem falar "Oi!".

**5 →** Altere o bloco "diga *Hello!* por 2 segundos" para "diga Oi! por 2 segundos". Clique no bloco alterado e veja o que acontece.

![Figura 2](https://i.imgur.com/DxN1X12.png)
*Figura 2*

Na parte direita da tela está uma área vazia. É nesse lugar que vamos juntar os blocos para fazer *scripts*². *Scripts* são a união de vários comandos. Os *scripts* que criaremos vão ser nosso programa!

² *script* - roteiro


### Personalizando o morcego

O morcego vai ficar atrapalhando nossa personagem em seu caminho para pegar o doce. Vamos fazer um programa para que o morcego fique se movendo de cima para baixo.

**1 →** Clique duas vezes no morcego.

**2 →** Pra começar, queremos que o morcego vá para cima. Clique no tipo de bloco "Movimento" e arraste o bloco "apontar para a direção 90 graus" para a área do programa (área do lado direito da tela).

**3 →** Clique na flechinha para baixo e escolha "(0) para cima".

**4 →** Para manter a direção do morcego mas impedir ele de girar, arraste o bloco "mude o estilo de rotação para..." para a área do programa. Clique na flecha para baixo e selecione "não girar". Assim, garantimos que o morcego não vai girar de cabeça para baixo ou para os lados.

**5 →** Arraste um bloco "mova 10 passos" para seu programa. Clique no bloco e veja o que acontece.

![Figura 3](https://i.imgur.com/u9c7jot.png)
*Figura 3*

Nosso morcego agora se movimenta um pouco, mas logo depois para. Vamos agora fazer com que o morcego continue se movendo.

**6 →** Clique no tipo de bloco "Controle". Arraste um bloco "sempre" para o programa. Esse bloco vai repetir um comando repetidas vezes até que o jogo termine.

**7 →** Arraste o bloco "mova 10 passos" para dentro do bloco "sempre". O que acontece quando você clica nele?

**8 →** O morcego se moveu para cima até chegar no limite da tela. Tente arrastar o morcego novamente para o centro da tela. Para parar o comando que move o morcego, clique no botão vermelho, ao lado da bandeira verde. Agora sim, você conseguirá mover o morcego para o centro da tela!

**9 →** Clique no tipo de bloco "Movimento". Arraste um bloco "se tocar na borda, volte" para dentro do bloco "sempre". Clique no bloco e veja o que acontece.

**10 →** Para fazer com que o morcego ande um pouco mais devagar, mude o bloco "mova 10 passos", substituindo o 10 por 5. Veja como ficou agora!

Mas como todos esses comandos funcionam? Quando rodamos o bloco "sempre", acontece o seguinte:

* O morcego move 5 passos.
* O programa verifica se o morcego tocou na borda. Se tiver tocado na borda, ele volta a direção. Se não, não faz nada.
* Como esses comandos estão dentro do bloco "sempre", eles vão continuar repetindo: move, verifica se tocou na borda, muda ou não a direção, move, verifica se tocou na borda, muda ou não a direção, move...

Com isso, aprendemos dois conceitos muito usados em programação: o conceito de **condição** e o conceito de **repetição**.

**Condição** é alguma coisa que depende de outra para acontecer. No caso do nosso jogo, a condição é: se o ator tocar na borda, ele salta de volta. Se não, continua para o próximo passo. Então para a volta acontecer, **depende** do ator tocar na borda. Isso é utilizado em vários programas que usamos normalmente. Por exemplo: toda vez que você abre o *site* *Facebook*, o código por trás do *Facebook* verifica se você tem novas solicitações de amizade. Se você tiver uma solicitação, vai aparecer uma indicação no ícone de amigos. Se não tiver novas solicitações, nada aparece.

**Repetição** é quando algo acontece repetidamente enquanto o programa está rodando. No caso do nosso jogo, o morcego continua se movendo e verificando se tocou na borda repetidamente. Isso também é usado em outros casos, como no jogo Tetris, em que as peças continuam caindo infinitamente enquanto o jogo está rodando.

**11 →** Clique no tipo de bloco "Eventos" e arraste o bloco "quando clicar em 🏳" para seu programa. Este comando é utilizado para definir o que acontece quando alguém clica na bandeira, ou seja, quando o jogo começa.

**12 →** Agora é hora de juntar os comandos que programamos para o morcego! Arraste os blocos para que fiquem na seguinte ordem, todos juntos: "quando clicar em 🏳", "mude o estilo de rotação...", "aponte para a direção..." e "sempre" (com os outros blocos dentro). O seu *script* deve se parecer com a Figura 4.

![Figura 4](https://i.imgur.com/MgjFUGF.png)
*Figura 4*

**13 →** Pare seu programa clicando no botão vermelho e reinicie clicando na bandeira verde.

Estamos prontas para a próxima etapa!


## Dando vida à personagem principal

Agora vamos fazer um programa para sua personagem se mover.

**1 →** Na seção que diz "Atores", clique na imagem da sua personagem.

**2 →** Clique no pequeno "i" no canto da imagem. Na parte que diz "estilo de rotação", selecione a opção **↔**. Então clique na flecha de voltar, que está no canto de cima. Isso vai fazer com que a imagem da personagem possa inverter de um lado pro outro, mas impede que ela gire em todos os sentidos.

![Figura 5](https://i.imgur.com/yn0CNkh.png)
*Figura 5*

Vamos escrever um *script* que vai permitir usar o teclado para mover nossa personagem.

**3 →** Clique no tipo de bloco "Eventos". Arraste para o programa um bloco "quando a tecla espaço for pressionada".

**4 →** Clique na flecha para baixo e mude de "espaço" para "seta para direita".

Como nossa personagem deve se comportar quando o evento "seta para direita é pressionada" acontece? Ela deve se mover para a direita. Vamos adicionar os comandos para isso acontecer!

**5 →** Vá para o tipo de bloco "Movimento". Arraste um bloco "aponte para a direção..." para o programa, encaixando embaixo do bloco "quando a tecla seta para a direita for pressionada". Clique na flechinha do bloco e escolha a opção "(90) direita". Precisamos desse comando para dizer ao programa para que direção nossa personagem vai se mover quando a tecla em questão for pressionada.

**6 →** Arraste um bloco "mova 10 passos" e junte aos outros blocos.

Agora experimente pressionar a tecla para direita do teclado. O que acontece? E se você pressionar a tecla para esquerda? Nada acontece. Nós precisamos adicionar ao nosso programa comandos para mover a personagem também quando as teclas para esquerda, para cima e para baixo são pressionadas. Nos próximos passos, vamos chegar a um programa como o da Figura 6.

![Figura 6](https://i.imgur.com/Zf4GW2C.png)
*Figura 6*

**7 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para cima. Troque as opções para formar os comandos "quando a tecla **seta para cima** for pressionada" e "aponte para a direção **0** graus".

**8 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para cima. Troque as opções para formar os comandos "quando a tecla **seta para baixo** for pressionada" e "aponte para a direção **180** graus".

**9 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para cima. Troque as opções para formar os comandos "quando a tecla **seta para a esquerda** for pressionada" e "aponte para a direção **-90** graus".

Agora experimente usar as teclas de todas as direções e veja o que acontece!

## Vencendo e Perdendo

Neste momento, nosso jogo permite que a personagem se mova, mas nada acontece se ela chegar no doce ou se o morcego alcançar ela. Vamos escrever um *script* para lidar com isso!

**1 →** Com sua personagem ainda selecionada, vá para os blocos do tipo "Eventos". Arraste o bloco "quando clicar em 🏳" para o programa.

**2 →** Vá para os blocos do tipo "Controle". Arraste um bloco "sempre" e cole no bloco anterior.

**3 →** Agora arraste um bloco "se __ então" para dentro do bloco "sempre".

**4 →** Vá para os blocos do tipo "Sensores". Arraste um bloco "tocando em ponteiro do mouse?" para a lacuna do bloco "se __ então".

**5 →** Clique na flechinha do bloco "tocando em..." e escolha a opção "Doce".

**6 →** Vá para o tipo de bloco "Aparência". Arraste um bloco "diga *Hello!* por 2 segundos" para dentro do bloco "se __ então". Mude de "*Hello!*" para "Você venceu!" e de 2 para 1 segundo.

**7 →** Para que o jogo termine, vá para o tipo de bloco "Controle" e arraste o bloco "pare todos" para dentro do "se __ então".

![Figura 7](https://i.imgur.com/EzRK40T.png)
*Figura 7*

Agora é hora de testar! Clique na bandeira e mova a personagem até o doce com o teclado. O que acontece?

Como você pôde ver, toda vez que clicamos na bandeira para começar o jogo, nosso programa fica **repetitivamente** verificando se a personagem está tocando no doce. Se a nossa **condição** for verdade, a personagem vence. Estamos usando novamente os conceitos de repetição e condição!

Quando nossa personagem encosta no morcego ou no vilão, ela deve perder o jogo. Vamos fazer isso acontecer!

**8 →** Adicione um bloco "se __ então" dentro do bloco "sempre".

Queremos que a personagem perca se **tocar o morcego OU tocar o vilão**. Como temos dois eventos que disparam um comportamento, vamos usar um **operador lógico**. Assim, em uma só condição, vamos disparar um efeito que vale para duas situações.

**9 →** Vá para o tipo de bloco "Operadores". Arraste um bloco "__ ou __ " para a lacuna "se __ então".

**10 →** Vá para o tipo de bloco "Sensores". Adicione um bloco "tocando em..." para cada lacuna do bloco "__ ou __ ".

**11 →** Em um dos blocos "tocando em...", substitua "ponteiro do mouse" por "Morcego". No outro, substitua "ponteiro do mouse" por "Vilão".

**12 →** Para dentro do bloco "se __ então", arraste um bloco "diga *Hello!* por 2 segundos" e faça as alterações para ter "diga Você perdeu! por 1 segundos".

**12 →** Depois de "diga Você perdeu! por 1 segundos", adicione um bloco "pare todos".

Depois dos passos anteriores, os comandos vão ser como os da imagem abaixo.

![Figura 8](https://i.imgur.com/Q2AgXim.png)
*Figura 8*

Agora reposicione novamente a personagem, aperte na bandeira e veja o que acontece quando nossa personagem encosta em um dos objetos letais.


## Tornando o vilão mais perigoso

Por enquanto, o vilão não é uma ameaça muito grande. Vamos fazer ele perseguir a personagem principal e tornar o jogo mais divertido!

**1 →** Clique na imagem do vilão.

**2 →** Vá para os blocos do tipo "Eventos". Arraste o bloco "quando clicar em 🏳" para o programa.

**3 →** Vá para os blocos do tipo "Controle". Arraste um bloco "sempre" para o programa, anexado ao bloco anterior.

**4 →** Vá para os blocos "Movimento". Arraste um bloco "aponte para ponteiro do mouse" para dentro do bloco "sempre". Clique na flechinha do bloco e escolha sua personagem.

**5 →** Arraste um bloco "mova 10 passos" e coloque dentro do bloco "sempre".

Aperte na bandeira para testar o jogo. Você vai ver que o vilão alcança a personagem rapidamente. Vamos mudar isso!

**6 →** Mude a quantidade de passos que o vilão anda de "mova 10 passos" para "mova 2 passos".

O *script* do vilão deve se parecer com a imagem abaixo:

![Figura 9](https://i.imgur.com/flQ3HK3.png)
*Figura 9*

## Finalizando

O jogo já está pronto para ser jogado. Isso significa que você construiu seu primeiro programa! Parabéns!

Agora, é hora de customizar ele com o que aprendemos por enquanto e descobrir novas funcionalidades do Scratch. Tente incrementar o seu jogo. Abaixo, algumas sugestões do que ainda pode ser feito. Use a imaginação!

* Tente adicionar um efeito de som quando a personagem alcança o bolo.
* Tente definir uma posição fixa para todos os objetos. Assim, não é preciso reposicionar cada um antes de jogar novamente.
* Tente adicionar mais inimigos ou obstáculos para que o jogo seja ainda mais desafiador.

Quando puder, explore mais o *site* do Scratch! Há diversos projetos muito interessantes por lá.

# O que faremos amanhã?

Hoje aprendemos muitas coisas! Desde uma parte da história da computação até conceitos de programação.

Amanhã, vamos continuar nosso aprendizado, principalmente focando em **construir páginas para Internet**. Vamos conhecer HTML e CSS, linguagens que vão permitir que você mesma construa sua página. Nosso objetivo vai ser conhecer alguns elementos que podem ser usados e como estilizá-los.

Para que essa página fique a sua cara, você tem um tema de casa:

**1 →** **Qual é o tema** que você quer abordar na sua página? (O formato vai ser em *blog* ou página informativa.)

**2 →** Quais conteúdos você deseja mostrar? Fotos? Gifs? Se preferir, **separe algumas imagens digitais** para usar em sua página.

Traga suas ideias!

-----------

# Revisão do primeiro dia

Antes de começar a aprender sobre novos assuntos, vamos rever os temas que vimos ontem.

* A importância das mulheres no início da computação e como surgiu a lacuna de gênero na TI
* O que são algoritmos?
* Qual a utilidade das linguagens de programação?
* Conceitos sobre o Scratch

Agora sim, vamos explorar novas ferramentas e ver o que podemos fazer com elas!

# HTML

HTML é uma linguagem usada para criar *websites*. A sigla HTML significa *HyperText Markup Language*³. É a linguagem que todos os navegadores que utilizamos (como o Google Chrome ou o Internet Explorer) conseguem interpretar. Ou seja, por trás de todas as páginas na Internet, há código HTML.

³ *HyperText Markup Language* - Linguagem de Marcação de Hipertexto

Vamos ver como isso funciona?

**→** Abra o navegador e entre na seguinte página da Wikipédia: [https://pt.wikipedia.org/wiki/Ada_Lovelace](https://pt.wikipedia.org/wiki/Ada_Lovelace).

**→** Com o mouse em qualquer lugar da página, clique com o botão direito e selecione a opção "Exibir código fonte da página".

Nesse momento, você deve ver o código HTML que está por trás desta página. Cada texto, *link* ou imagem está em algum lugar deste código. Agora pode parecer confuso, mas em breve você entenderá o significado de grande parte do que você está vendo.

O HTML tem várias regras determinadas para que o navegador consiga interpretar o que deve ser mostrado em cada parte da página. Por isso, devemos seguir uma certa estrutura quando estamos escrevendo uma página HTML. Essa estrutura é baseada em elementos, que são especificados por diferentes tags. Uma tag simples é formada da seguinte maneira:

![Imagem tag](https://i.imgur.com/NkxOJ49.jpg)

Um elemento pode ser um texto, uma caixa, uma lista. Nos próximos itens, vamos ver como esses elementos e suas tags são utilizados enquanto criamos nossa primeira página HTML!

## Criando um arquivo HTML

Vamos criar um arquivo HTML para poder criar a página.

**1 →** Abra o programa Sublime Text.*

\* Saiba mais sobre o Sublime Text na seção **Algumas Referências**.

**2 →** Clique em "Arquivo" (opção na barra de cima) e depois "Novo Arquivo".

**3 →** Clique novamente em "Arquivo" e depois "Salvar".

**4 →** Selecione uma pasta para salvar seu arquivo.

**5 →** Na caixa para escrever o nome do arquivo, escreva *site.html*.

**6 →** Clique em salvar

Agora que já temos um arquivo *site.html*, vamos ver como criamos conteúdo dentro dele.

## Estrutura de uma página HTML

Para cada página que criamos, há algumas *tags*⁴ básicas que sempre iremos utilizar.

⁴ *tag* - etiqueta; rótulo

### A tag html

A *tag* `<html>` define o início e fim de uma página HTML. Dentro dela, é sempre necessário declarar duas *tags*: `<head>`⁵ e `<body>`⁶. Para definir a hierarquia entre esses elementos, dizemos que `<html>` é a *tag* "pai" de `<head>` e `<body>`, e que essas duas são *tags* "irmãs". Observe o trecho de código HTML abaixo: a posição das tags demonstra essa hierarquia.

⁵ *head* - cabeçalho
⁶ *body* - corpo

```html
<html>
  <head></head>
  <body></body>
</html>
```

**→** Escreva exatamente o texto acima dentro do seu arquivo.

**Dica:** **Indentação** é o uso de espaçamentos antes das *tags* que ajudam na visualização de quais elementos estão "dentro" de outros. A tecla *tab* pode te ajudar com a indentação do seu código HTML. Toda vez que você quiser aumentar o espaçamento, pressione *tab*.

### A tag head

A *tag* `<head>` é o cabeçalho, é o elemento que guarda as configurações de uma página HTML. Dentro desse cabeçalho, devemos ter sempre a *tag* `<title>`⁷, que é o título da página, e vai aparecer na barra de título da janela do navegador.

⁷ *title* - título

```html
<html>
  <head>
    <title>Meu primeiro site</title>
  </head>

  <body></body>
</html>
```

**→** Adicione a *tag* `<title>` com o conteúdo ao seu arquivo HTML, assim como mostra o código acima.

Outra *tag* de configuração importante é `<meta>`. Essa *tag* serve para definir qual tipo de caracteres será utilizado na página. Vamos utilizar a configuração UTF-8, que permite que usemos caracteres com acentos e cedilhas na nossa página.

```html
<html>
  <head>
    <title>Meu primeiro site</title>
    <meta charset="utf-8">
  </head>

  <body></body>
</html>
```

**→** Adicione a *tag* `<meta>` ao seu arquivo HTML, assim como mostra o código acima.

Observe que na *tag* `<meta>`, não temos nenhum conteúdo, mas sim algo que chamamos de **atributo**, que é uma especificação daquele elemento.

### Instrução DOCTYPE

 `<!DOCTYPE html>` -  essa não é uma *tag* HTML, mas sim uma instrução especial, que define que estamos utilizando a última versão do HTML.

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Meu primeiro site</title>
    <meta charset="utf-8">
  </head>

  <body></body>
</html>
```

**→** Adicione a instrução `<!DOCTYPE html>` no começo do seu arquivo HTML, assim como mostra o código acima.

Para ver como a página está até agora, vamos abrir o arquivo HTML no navegador.

**1 →** Abra o navegador de Internet.

**2 →** No menu do navegador (barra em cima da página), clique em "Arquivo" e depois em "Abrir...".

**3 →** Selecione o arquivo HTML que você criou.

Observe o título da página. Essa é sua primeira alteração visível!

## Adicionando elementos na página

Agora que já definimos a estrutura da página com as tags básicas que aprendemos, vamos ver as *tags* que vão adicionar elementos na tela. Essas novas *tags* de elementos devem ficar dentro do elemento `<body>`.

### Título

Como usar:

```html
<h1>Algum título</h1>
```

Para definir títulos, usamos as *tags* *heading*⁸. A *tag* `<h1>` define os títulos mais importantes (maiores), enquanto a *tag* `<h6>` define os que são menos importantes (menores).

⁸ *heading* - linha de título

![Hierarquia de títulos](https://i.imgur.com/pnmIy0i.png?1)

**→** Coloque um título em sua página adicionando dentro do `body` a seguinte *tag*: `<h1>Como criei meu primeiro site</h1>`

Seu código deve ficar assim:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Meu primeiro site</title>
    <meta charset="utf-8">
  </head>

  <body>
    <h1>Como criei meu primeiro site</h1>
  </body>
</html>
```

### Parágrafo

Como usar:

```html
<p>Algum texto que será seu parágrafo.</p>
```

Escrever textos em forma de parágrafo, faz com que o usuário leia a página de forma mais fácil.

**→** Dentro do `<body>`, depois do `<h1>`, adicione a *tag* `<p>` para ter um parágrafo:

```html
<body>
  <h1>Como criei meu primeiro site</h1>
  <p>
    Esse é meu primeiro site. Fiz ele utilizando HTML,
    que é uma linguagem usada para criar websites. A
    sigla significa HyperText Markup Language
    (Linguagem de Marcação de Hipertexto). É a
    linguagem que todos os navegadores (como o Google
    Chrome ou o Internet Explorer) conseguem
    interpretar.
  </p>
</body>
```

### Formatação

Como usar:

```html
<strong>Esse texto será em negrito.</strong>
```

Para dar destaque ou um significado diferenciado a algumas partes de um texto, podemos usar *tags* de formatação. Para ver como isso funciona, siga o próximo passo:

**→** No parágrafo que acabamos de adicionar, substitua as palavras `meu primeiro site` por `<strong>meu primeiro site</strong>`.

Além da *tag* `<strong>`⁹, que deixa o texto em negrito, há também a *tag* `<em>` (abreviação de *emphasized*¹⁰), que deixa o texto em itálico, a *tag* `<small>`¹¹, que diminui o texto e a *tag* `<big>`¹², que aumenta. Experimente usar essas outras *tags* em seu texto e veja o que acontece.

⁹ *strong* - forte

¹⁰ *emphasized* - enfatizado

¹¹ *small* - pequeno

¹² *big* - grande

### Link

Como usar:

```html
<a href="algum-site.com">Texto que será exibido</a>
```

*Links* são a maneira de fazer com que os usuários naveguem para outras páginas através de um *site*. A *tag* para criar *links* é `<a>` (abreviação de *anchor*¹³) e utilizamos o atributo `href` (*Hypertext Reference*¹⁴) para especificar para qual página queremos levar o usuário.

¹³ *anchor* - âncora

¹⁴ *Hypertext Reference* - referência de hipertexto

**→** Adicione o seguinte *link* ao final do seu parágrafo (antes de fechar a *tag* `<p>`): `<a href="https://pt.wikipedia.org/wiki/HTML">Saiba mais sobre HTML...</a>`

**Dica:** Se você deseja que o *link* fique na linha de baixo, adicione a *tag* `<br/>` antes do *link*. O nome da *tag* vem de *line break*¹⁵.

¹⁵ *line break* - quebra de linha

### Bloco de divisão

Como usar:

```html
<div>
  <h1>Título</h1>
  <p>Outros elementos podem ser colocados aqui.</p>
</div>
```

A *tag* `<div>` permite agrupar elementos e definir um "bloco" ali. Isso vai ser muito útil mais tarde, quando quisermos adicionar estilo (como uma cor de fundo) a um grupo de elementos.

**→** Adicione uma `<div>` ao redor do seu título e parágrafo. Seu código deve ficar como abaixo:

```html
<body>
  <div>
    <h1>Como criei meu primeiro site</h1>
    <p>
      Esse é meu primeiro site. Fiz ele utilizando HTML,
      que é uma linguagem usada para criar websites. A
      sigla significa HyperText Markup Language
      (Linguagem de Marcação de Hipertexto). É a
      linguagem que todos os navegadores (como o Google
      Chrome ou o Internet Explorer) conseguem
      interpretar.
      <br/>
      <a href="https://pt.wikipedia.org/wiki/HTML">
        Saiba mais sobre HTML...
      </a>
    </p>
  </div>
</body>
```

### Lista

Como usar:

```html
<ul>
  <li>Item A</li>
  <li>Item B</li>
  <li>Item C</li>
</ul>
```

Para adicionar listas, usamos a *tag* `<ul>` (*unordered list*¹⁶). Para cada elemento da lista, usamos a *tag* `<li>` (*list item*¹⁷). Vamos criar um outro título e adicionar uma lista à página.

¹⁶ *unordered list* - lista desordenada

¹⁷ *list item* - item da lista

**→** Adicione o seguinte código depois de `</p>`:

```html
<h2>O que eu já aprendi sobre HTML</h2>
<ul>
  <li>Estrutura básica</li>
  <li>Títulos</li>
  <li>Links</li>
</ul>
```

Veja no navegador como ficou agora a página.

Também podemos adicionar **listas ordenadas** a uma página. Troque a *tag* `<ul></ul>` por `<ol></ol>` (*ordered list*¹⁸) e veja qual é a mudança que acontece.

¹⁸ *ordered list* - lista ordenada

### Imagem

Como usar:

```html
<img src="local-imagem.jpeg" alt="Descrição da imagem">
```

Agora que já aprendemos a adicionar texto em vários formatos, vamos aprender a adicionar imagens em uma página!

Primeiramente, precisamos salvar a imagem que queremos no computador.

**1 →** Crie uma pasta com o nome "imagens" dentro da mesma pasta em que está seu arquivo HTML.

**2 →** Com o navegador, acesse o *site* [https://i.imgur.com/QhWITAr.png](https://i.imgur.com/QhWITAr.png).

**3 →** Clique na imagem com o botão direito e selecione "Salvar imagem como…".

**4 →** Selecione a pasta "imagens" que você criou e nomeie o arquivo como "imagem1".

Um elemento de imagem é formado por três partes:

* A *tag* `<img>`
* O atributo `src` (abreviação de *source*¹⁹), que é onde colocamos o "caminho" para a imagem que queremos
* O atributo `alt` (abreviação de *alternative information*²⁰), que provê uma descrição da imagem.

¹⁹ *source* - origem

²⁰ *alternative information* - informação alternativa

Adicione o seguinte código em sua página, antes da *tag* `<div>`: `<img src="imagens/imagem1.png" alt="Capa do site">`.

## Finalizando

Com cada passo que completamos, o conteúdo final do arquivo *site.html* é o seguinte:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Meu primeiro site</title>
    <meta charset="utf-8">
  </head>

  <body>
    <img src="imagens/imagem1.png" alt="Capa do site">
    <div>
      <h1>Como criei meu primeiro site</h1>
      <p>
        Esse é meu primeiro site. Fiz ele utilizando HTML,
        que é uma linguagem usada para criar websites. A
        sigla significa HyperText Markup Language
        (Linguagem de Marcação de Hipertexto). É a
        linguagem que todos os navegadores (como o Google
        Chrome ou o Internet Explorer) conseguem
        interpretar.
        <br/>
        <a href="https://pt.wikipedia.org/wiki/HTML">
          Saiba mais sobre HTML...
        </a>
      </p>

      <h2>O que eu já aprendi sobre HTML</h2>
      <ul>
        <li>Estrutura básica</li>
        <li>Títulos</li>
        <li>Links</li>
      </ul>
    </div>
  </body>
</html>
```

E a página que o navegador mostra é como a imagem abaixo:

![Página mostrada pelo navegador](https://i.imgur.com/NSr9aF5.png)

E assim acabamos de construir nossa primeira página HTML!


# CSS

Quando estamos escrevendo HTML, cada *tag* representa um elemento específico. Para cada elemento, o navegador tem uma exibição diferente. Um título `<h1>`, por exemplo, fica em negrito e em uma fonte maior. Porém, para fazer *sites* com cores, tamanhos e espaçamentos diferenciados, essa estilização padrão não é suficiente. Para isso, precisamos customizar o estilo da página, e esse é exatamente o objetivo do CSS.

O CSS é uma linguagem para estilizar páginas na Internet. A sigla significa *Cascading Style Sheets*²¹. Enquanto o HTML é responsável pela estruturação dos elementos de uma página na Internet, o CSS é responsável pela apresentação customizada desses elementos.

²¹ *Cascading Style Sheets* - Folhas de estilo em cascata

## Estrutura de um arquivo CSS

Um arquivo CSS é formado por várias regras. Uma regra tem o seguinte formato:

```css
h1 {
  color: blue;
}
```

* `h1` é o **seletor**, é o que indica em qual elemento será aplicada a regra. Nesse caso, estamos selecionando o título.
* `color`²² é a **propriedade**, é o que define que aspecto do elemento será mudado. Nesse caso, vamos mudar a cor.
* `blue`²³ é o **valor** correspondente à propriedade. Nesse caso, vamos usar a cor azul.

²² *color* - cor
²³ *blue* - azul

Vamos aprender como CSS funciona na prática?

## Criando um arquivo CSS

Primeiro, vamos criar nossa folha de estilo.

**→** Na mesma pasta onde está seu arquivo HTML, crie um arquivo CSS e o nomeie como *estilo.css*.

No arquivo criado, vamos colocar as regras de estilo para nossa página HTML. Para a página reconhecer esse estilo, siga os passos:

**1 →** Abra seu arquivo HTML.
**2 →** Dentro da *tag* `<head>`, adicione a seguinte linha: `<link rel="stylesheet" href="estilo.css">`.

## Adicionando regras de estilo

Agora que temos uma folha de estilo para nossa página, vamos criar algumas regras. A primeira delas será para mudar a fonte.

**→** Adicione o seguinte trecho ao seu arquivo *estilo.css*:

```css
body {
  font-family: Helvetica, Arial, sans-serif;
}
```

²⁴ *font-family* - família de fontes

Você consegue identificar qual é o seletor nesta regra? E a propriedade? E o valor?

Observe também onde a regra que criamos foi aplicada e como isso se reflete na página. Quando selecionamos um elemento, a regra será aplicada a todos os elementos "aninhados" dentro dele.

Agora, vamos remover os pontos à esquerda de cada item da lista que criamos.

**→** Adicione o seguinte trecho ao seu arquivo *estilo.css*²⁵:

```css
ul {
  list-style: none;
}
```

²⁵ *list-style* - estilo de lista | *none* - nenhum

Quando estamos definindo o CSS, podemos adicionar várias propriedades em uma só regra. Tudo que fica entre as chaves `{}`, são propriedades. Veja abaixo como isso funciona.

**→** Para definir um estilo diferente para os *links*, adicione o seguinte²⁶:

```css
a {
  color: #37a2f7;
  text-decoration: none;
  border-bottom: 1px dotted #37a2f7;
}
```

²⁶ *text-decoration* - decoração do texto | *border-bottom* - borda inferior | *dotted* - pontilhado

O que cada propriedade e valor significam? Abaixo, uma explicação sobre cada propriedade:

### color

Essa é a propriedade para definir **cor**. Nesse caso, estamos definindo que a cor é um tipo de azul, representado pelo código `#37a2f7`. Para cada cor diferente, há um **código hexadecimal** diferente. Para algumas cores, podemos substituir o código pelo nome da cor. Veja algumas cores nomeadas abaixo:

![Lista de cores](https://i.imgur.com/xSCsC3t.png)

Para saber mais sobre cores em páginas HTML, acesse o endereço [http://html-color-codes.info/Codigos-de-Cores-HTML/](http://html-color-codes.info/Codigos-de-Cores-HTML/).

### text-decoration

A propriedade *text-decoration* permite que mudemos o estilo dos textos. Alguns valores que podem ser usados são: *underline*²⁷, *overline*²⁸ e *line-through*²⁹. Todos os *links*, por padrão, já vem com algumas decorações. Nesse caso, estamos usando *none* para remover essas decorações padrão.

²⁷ *underline* - sublinhado
²⁸ *overline* - sobre-linha
²⁹ *line-through* - linha atravessada

### border-bottom

Essa propriedade define o estilo da linha de baixo de um elemento. Todas as propriedades de borda podem ser definidas da seguinte forma: `border-bottom: [espessura] [estilo] [cor]`. No nosso caso, a espessura é 1 pixel, o estilo é pontilhado e a cor é igual a cor da fonte.

### line-height

**→** Adicione a propriedade *line-height*³⁰ ao seletor `body` para deixar o texto da página com um espaçamento maior.

³⁰ *line-height* - espessura de linha

```css
body {
  font-family: Helvetica, Arial, sans-serif;
  line-height: 1.3;
}
```

## Outros tipos de seletores

Até então, estamos selecionamos os itens de uma regra CSS por *tag*. Quando selecionamos por *tags*, todos os elementos que forem daquela *tag* também terão seus estilos alterados. Para que possamos selecionar elementos de outras maneiras, há outros tipos de seletores. Vamos conhecê-los!

### Seletor por id

Quando queremos estilizar apenas um item, e não todos os elementos de uma mesma tag, podemos usar um *id*. Um *id* é uma **identificação** de um elemento, e serve para adicionar regras a um elemento específico.

Veja o seguinte código HTML e a folha de estilo abaixo³¹.

```html
<img id="logomarca" src="images/logo.png" alt="Meu logo">
```

```css
#logomarca {
  margin: 0 auto 30px;
  width: 200px;
}
```

³¹ *margin* - margem | *width* - largura

Com essa regra, estamos alterando somente a imagem que é o logo da página, ao invés de estilizar todas as imagens.

A propriedade `margin` define o espaço mínimo de distância entre o elemento e os elementos ao seu redor. O valor dessa propriedade é formado por 4 partes: margem superior, margem direita, margem inferior e margem esquerda. Dado nosso exemplo de `margin: 0 auto 30px;`, sabemos que acima do logo não haverá margem alguma, na direita a margem será automática, abaixo do logo terá uma margem de 30px e na esquerda a margem também será automática (pois quando não especifica-se a margem, ela é automática). As margens automáticas nos lados, fazem com que a logomarca fique ao centro da página.

A propriedade `width`, define a largura, ou seja, o tamanho horizontal da imagem.

### Seletor por classe

Quando queremos estilizar diferentes elementos da mesma maneira, podemos usar classes.

```html
<ul>
  <li>Arroz</li>
  <li class="fruta">Morango</li>
  <li class="fruta">Banana</li>
  <li>Chocolate</li>
  <li class="fruta">Laranja</li>
</ul>
```

```css
.fruta {
  color: Tomato;
  margin-left: 5px;
}
```

![Renderização HTML](https://i.imgur.com/4GKeTun.png)

### Diferença entre *id* e *class*

Os seletores ***id* e *class*** funcionam praticamente da mesma maneira. A diferença entre eles é quando usar cada um. O *id* é utilizado para definir elementos únicos. Isso significa que *ids* devem ser sempre únicos na página. Já as classes, servem para definir uma classificação dos elementos. Com elas é possível ter mais flexibilidade e reutilizar uma regra para vários elementos.

Quando estamos querendo estilizar uma imagem no topo da página, por exemplo, usamos um *id*, pois sabemos que aquele elemento é único e a regra só vai ser aplicada a ele. Já quando vamos estilizar os comentários de uma página, usamos *class*, pois sabemos que vão existir vários comentários em uma mesma página e a regra deve ser aplicada a todos eles.

Além disso, um elemento pode ter diferentes *ids* e classes, aplicando estilos de várias regras do CSS ao mesmo tempo. Veja o exemplo abaixo:

```html

<ul id="caixa-comentarios" class="caixa-lateral">

  <li class="comentario">Algum comentário</li>

  <li class="comentario">Mais um comentário</li>

  <li class="comentario">Último</li>
</ul>


```

### Seletores em cascata

Também podemos delimitar os elementos de uma regra usando seletores em cascata, que selecionam elementos de forma hierárquica.

No exemplo abaixo³², o elemento "pai" rodapé é selecionado pelo seu *id*. O estilo será aplicado apenas nos elementos *img* que estão dentro do rodapé.

```css
#rodape img {
  margin-right: 35px;
  vertical-align: middle;
  width: 94px;
}
```

³² *margin-right* - margem direita | *vertical-align* - alinhamento vertical

## Voltando à página

Vamos agora aplicar o que aprendemos nos últimos itens!

### Centralizando o conteúdo

Para centralizar o conteúdo do *site*, precisamos encapsular este conteúdo em uma *div* e depois adicionar o estilo.

**1 →** Adicione o *id* "conteudo" na *div* existente em seu arquivo HTML, como mostrado abaixo:

```html
<body>
  <img src="imagens/imagem1.png" alt="Capa do site">
  <div id="conteudo">
    <h1>Como criei meu primeiro site</h1>
    ...
    ...
    ...
    </ul>
  </div>
</body>
```

**2 →** Adicione a seguinte regra ao arquivo CSS³³:

```css
#conteudo {
  width: 900px;
  margin: 0 auto 40px;
  padding: 0;
}
```

³³ *padding* - preenchimento

Estamos definindo que:
* a **largura** da área com conteúdo é de 900px
* que não há margem superior, a margem inferior é 40px e as margens laterais são automáticas
* que não há margem interna

A propriedade *padding* define o tamanho das margens **internas** de um elemento. Ou seja, o espaçamento entre a borda do elemento e seu conteúdo, como mostrado abaixo:

![Padding e Margin](https://i.imgur.com/vUAL3aF.png)

**3 →** Para fazer com que a capa ocupe toda a largura da página, adicione o *id* "capa" ao elemento da imagem de capa e acrescente a regra abaixo ao CSS:

```css
#capa {
  width: 100%;
}
```

### Personalizando imagens

Para aprender um pouco mais sobre como posicionar elementos em uma página HTML usando CSS, vamos usar algumas imagens.

**1 →** Adicione o seguinte trecho de código à página HTML, depois da lista:

```html
<h2>Como aprender sobre HTML na Internet</h2>

<img src="https://i.imgur.com/9F3Azcrl.png" alt="Tela com apostila HTML">
<img src="https://i.imgur.com/U1oEJ19l.png" alt="Site Udacity">
<img src="https://i.imgur.com/8L6IYzPl.png" alt="Busca YouTube">
```

**Dica:** Além de usar imagens armazenadas em seu computador, como fizemos na primeira imagem, também é possível usar diretamente as imagens que já estão em um endereço na Internet.

**2 →** Para melhorar o posicionamento das imagens, adicione a seguinte regra ao arquivo CSS:

```css
img.picture {
  width: 260px;
  margin: 3px;
}
```

Quais mudanças as regras adicionadas causaram nos elementos?

**3 →** Vamos deixar nossas imagens com uma borda diferenciada. Para isso, adicione as seguintes propriedades à regra que você acabou de criar³⁴:

```css
img.picture {
  margin: 3px;
  width: 260px;
  border: 2px solid #6d6d6d;
  border-radius: 5px;
}
```

³⁴ *solid* - sólido | *border radius* - raio da borda

Também podemos adicionar aos elementos algum efeito específico quando passamos com o mouse por cima. Vamos fazer isso com nossas imagens!

Para o efeito que vamos fazer, vamos deixar todas as imagens com uma opacidade baixa (um pouco mais apagadas), para quando passarmos o mouse ela ficar com opacidade completa.

**4 →** Para remover um pouco da opacidade, adicione a propriedade `filter: opacity(0.6);`³⁵ ao seletor `img.picture`.

³⁵ *filter* - filtro | *opacity* - opacidade

**5 →** Para deixar a opacidade completa, adicione a seguinte regra³⁶:

```css
img.picture:hover {
  filter: opacity(1);
}
```

³⁶ *hover* - pairar

Usamos a pseudo-classe *hover* para definir o estilo de um elemento quando o mouse está sobre ele.

**6 →** Para suavizar a transição, adicione a propriedade *transition*³⁷, definindo uma transição de 0,3 segundo:

```css
img.picture {
  margin: 3px;
  width: 260px;
  border: 2px solid #6d6d6d;
  border-radius: 5px;
  filter: opacity(0.6);
  transition: 0.2s;
}
```

³⁷ *transition* - transição

Assim como o filtro de opacidade, há muitos outros. Veja abaixo alguns filtros possíveis e experimente usá-los:

![Outros filtros de imagem](https://i.imgur.com/BhnAO7r.png)

Para saber mais sobre filtros de imagens, acesse: [http://www.maujor.com/tutorial/css3-filters.php](http://www.maujor.com/tutorial/css3-filters.php).

### Criando páginas com mais de uma coluna

Vamos criar uma coluna lateral no nosso site. Para isso, vamos separar a *div* "conteudo" em duas: uma com a classe "coluna-principal", que vai conter os elementos existentes e outra com a classe "coluna-lateral", que vai conter outros elementos como mostrado abaixo:

```html
  <body>
    <img id="capa" src="imagens/imagem1.png" alt="Capa do site">

    <div id="conteudo">
      <div class="coluna-principal">
        <h1>Como criei meu primeiro site</h1>
        ...
        <img src="https://i.imgur.com/8L6IYzPl.png" alt="Busca YouTube" class="picture">
      </div>

      <div class="coluna-lateral">
        <h4>Sobre a autora</h4>
        <p>
          Joana tem 15 anos e mora em Porto Alegre. Etiam porta sem malesuada magna mollis euismod. Cras mattis consectetur purus sit amet fermentum.
        </p>
      </div>
    </div>

  </body>
```

**1 →** Faça as alterações em sua página HTML para que fique com a estrutura mostrada acima.

**2 →** Para que as colunas fiquem em suas posições, adicione as seguintes regras:

```css
.coluna-principal {
  float: left;
  width: 70%;
}

.coluna-lateral {
  float: right;
  width: 20%;
  padding: 15px;
}
```

**3 →** Vamos melhorar o estilo da coluna lateral? Adicione as propriedades abaixo.

```css
.coluna-lateral {
  float: right;
  width: 20%;
  padding: 15px;
  background-color: #f5f5f5;
  border-radius: 4px;
  margin-top: 20px;
}
```

# Customizando seu próprio site

Com os conceitos de HTML e CSS aprendidos, você já construiu uma página *web*. Essa página é sua! Agora é hora de usar os recursos que você aprendeu e a sua criatividade para customizar sua página.

* Qual é o assunto que você quer abordar na página?
* Quais conteúdos você deseja mostrar? Um texto? Imagens? Que elementos são necessários para isso?
* Qual é o estilo que você deseja que a página tenha? Um plano de fundo mais divertido? Uma fonte diferente?

Veja abaixo algumas ideias de mudanças que você pode fazer.

#### Mudar a imagem no topo da sua página

Você pode escolher uma foto, uma imagem abstrata ou o que preferir. Lembre-se que é possível usar uma imagem salva no computador ou o link de uma imagem existente na Internet. Você também pode aplicar um filtro à sua nova imagem.

#### Utilizar ícones

Se você deseja utilizar ícones para em sua página, é possível! Veja como fazer isso em: https://www.w3schools.com/css/css_icons.asp.

#### Adicionar fotos do Instagram na sua página

Siga os passos do tutorial no link http://youngstudio.com.br/como-vincular-sua-conta-do-instagram-com-o-site/ e copie o código gerado na sua página.

# Próximos passos

Se você quer **aprender mais sobre Scratch**, o próprio site é muito didático. Vá para a página de Projetos para Iniciantes (https://scratch.mit.edu/starter_projects/) e divirta-se!

Se você deseja ver mais **exemplos e referências sobre HTML e CSS**, acesse o site https://www.w3schools.com/.

Existem também apostilas (https://www.caelum.com.br/apostila-html-css-javascript/introducao-a-html-e-css/) e vídeos online (https://www.youtube.com/watch?v=iZ1ucWosOww) que podem te ajudar nos próximos passos.

# Algumas Referências

http://gizmodo.uol.com.br/especial-a-mulher-no-mercado-de-tecnologia/

http://www.maujor.com/tutorial/css3-filters.php

http://www.npr.org/sections/money/2014/10/17/356944145/episode-576-when-women-stopped-coding

https://www.slideshare.net/JulianaDorneles1/mulheres-na-tecnologia-rails-girls-bh-2015

http://tutorials.codebar.io/html/lesson1/tutorial.html

https://www.w3schools.com/

Sobre o programa Sublime Text: http://www.devmedia.com.br/sublime-text-ide-introducao-a-melhor-ide-para-desenvolvimento/34117

# Glossário

*alternative information* - informação alternativa

*anchor* - âncora

*big* - grande

*blue* - azul

*body* - corpo

*border radius* - raio da borda

*border-bottom* - borda inferior

*Cascading Style Sheets* - Folhas de estilo em cascata

*color* - cor

*dotted* - pontilhado

*emphasized* - enfatizado

*filter* - filtro

*font-family* - família de fontes

*head* - cabeçalho

*heading* - linha de título

*hello* - olá

*hover* - pairar

*HyperText Markup Language* - Linguagem de Marcação de Hipertexto

*Hypertext Reference* - referência de hipertexto

*line break* - quebra de linha

*line-height* - espessura de linha

*line-through* - linha atravessada

*list item* - item da lista

*list-style* - estilo de lista

*margin-right* - margem direita

*margin* - margem

*none* - nenhum

*opacity* - opacidade

*ordered list* - lista ordenada

*overline* - sobre-linha

*padding* - preenchimento

*script* - roteiro

*small* - pequeno

*solid* - sólido

*source* - origem

*strong* - forte

*tag* - etiqueta; rótulo

*text-decoration* - decoração do texto

*title* - título

*transition* - transição

*underline* - sublinhado

*unordered list* - lista desordenada

*vertical-align* - alinhamento vertical

*width* - largura
