# HTML

HTML é uma linguagem usada para criar websites. A sigla HTML significa HyperText Markup Language (Linguagem de Marcação de Hipertexto). É a linguagem que todos os navegadores que utilizamos (como o Google Chrome ou o Internet Explorer) conseguem interpretar. Ou seja, por trás de todas as páginas na Internet, há código HTML.

Vamos ver como isso funciona?

**→** Abra o navegador e entre na seguinte página da Wikipédia: [https://pt.wikipedia.org/wiki/Ada_Lovelace](https://pt.wikipedia.org/wiki/Ada_Lovelace).

**→** Com o mouse em qualquer lugar da página, clique com o botão direito e selecione a opção "Exibir código fonte da página".

Nesse momento, você deve ver o código HTML que está por trás dessa página. Cada texto, link ou imagem está em algum lugar desse código. Agora pode parecer confuso, mas em breve você entenderá o significado de grande parte do que você está vendo.

O HTML tem várias regras determinadas para que o navegador consiga interpretar o que deve ser mostrado em cada parte da página. Por isso, devemos seguir uma certa estrutura quando estamos escrevendo uma página HTML. Essa estrutura é baseada em elementos, que são especificados por diferentes tags. Uma tag simples é formada da seguinte maneira:

![Imagem tag](https://i.imgur.com/NkxOJ49.jpg)

Um elemento pode ser um texto, uma caixa, uma lista. Nos próximos items, vamos ver como esses elementos e suas tags são utilizados enquanto criamos nossa primeira página HTML!

## Criando um arquivo HTML

Vamos criar um arquivo HTML para poder criar a página.

**1 →** Abra o programa Atom.

**2 →** Clique em *Arquivo* (opção na barra de cima) e depois *Novo Arquivo*

**3 →** Clique novamente em *Arquivo* e depois *Salvar*

**4 →** Selecione uma pasta para salvar seu arquivo

**5 →** Na caixa para escrever o nome do arquivo, escreva *site.html*

**6 →** Clique em salvar

Agora que já temos um arquivo site.html, vamos ver como criamos conteúdo dentro dele.

## Estrutura de uma página HTML

Para toda página que criamos, há algumas tags básicas que sempre iremos utilizar.

```html
<!DOCTYPE html>
<html>
<head>
	<title>Meu site!</title>
	<meta charset="utf-8">
</head>
<body>
	<p>Algum texto</p>
</body>
</html>
```

### A tag html

A tag `<html>` define o início e fim de uma página HTML. Dentro dela, é sempre necessário declarar duas tags: `<head>` e `<body>`. Para denifir a hierarquia entre essas tags, dizemos que `<html>` é a tag "pai" de `<head>` e `<body>`, e que essas duas são tags "irmãs". Observe o trecho de código HTML abaixo: posição das tags demonstra essa hierarquia.

```html
<html>
	<head></head>
	<body></body>
</html>
```

**→** Escreva exatamente o texto acima dentro do seu arquivo.

**Dica:** Identação é o uso de espaçamentos antes das tags que ajudam na visualização de quais tags estão "dentro" de outras. A tecla "tab" pode te ajudar com a identação do seu código HTML. Toda vez que você quiser aumentar o espaçamento, pressione "tab".

### A tag head

A tag `<head>`, que siginifica cabeçalho, guarda as configurações de uma página HTML. Dentro desse cabeçalho, devemos ter sempre a tag `<title>`, que é o título da página, e vai aparecer na barra de título da janela do navegador.

```html
<html>
	<head>
		<title>Meu primeiro site</title>
	</head>
	<body></body>
</html>
```

**→** Adicione a tag `<title>` com o contéudo ao seu arquivo HTML, assim como mostra o código acima.

Outra tag de confirguração importante é `<meta>`. Essa tag serve para definir a codificação dos caracteres. Vamos utilizar a configuração UTF-8, isso vai permitir que usemos caracteres com acentos e cedilhas no nosso site.

```html
<html>
	<head>
		<title>Meu primeiro site</title>
		<meta charset="utf-8">
	</head>
	<body></body>
</html>
```

**→** Adicione a tag `<meta>` ao seu arquivo HTML, assim como mostra o código acima.

Observe que na tag `<meta>`, não temos nenhum conteúdo, mas sim algo que chamamos de *atributo*, que é uma especificação daquele elemento.

### Instrução DOCTYPE

 `<!DOCTYPE html>` -  essa não é uma tag HTML, mas sim uma instrução especial, que define que estamos utilizando a última versão do HTML.

```html
<!DOCTYPE html>
<html>
	<head>
		<title>Meu primeiro site</title>
		<meta charset="utf-8">
	</head>
	<body></body>
</html>
```

**→** Adicione a instrução `<!DOCTYPE html>` no começo do seu arquivo HTML, assim como mostra o código acima.

Para ver como sua página está até agora, abra o arquivo HTML no navegador.

**1 →** Abra o navegador de Internet

**2 →** No menu do navegador (barra em cima da página), vá em *Arquivo*, depois *Abrir...*.

**3 →** Selecione o arquivo HTML que você criou.

Observe o título da página. Essa é sua primeira alteração visível!

## Adicionando elementos na página

Agora que já definimos a estrutura da página com as tags básicas que aprendemos, ver as tags que vão adicionar elementos na tela. Essas tags devem ficar dentro da tag `<body>`.

### Títulos

Para definir títulos, usamos as tags "heading". A tag `<h1>` define os títulos mais importantes (maiores), enquanto a tag `<h6>` define os menos importantes (menores).

![Hierarquia de títulos](https://i.imgur.com/pnmIy0i.png?1)

**→** Coloque um título em sua página adicionando dentro do `body` a seguinte tag: `<h1>Como criei meu primeiro site</h1>`

Seu código deve ficar assim:

```html
<!DOCTYPE html>
<html>
	<head>
		<title>Meu primeiro site</title>
		<meta charset="utf-8">
	</head>
	<body>
	  <h1>Como criei meu primeiro site</h1>
	</body>
</html>
```

### Parágrafos

Escrever textos em forma de parágrafo, faz com que o usuário leia a página de forma mais fácil.

**→** Dentro do `<body>`, depois do `<h1>`, adicione a tag `<p>` para ter um parágrafo:

```html
<body>
  <h1>Como criei meu primeiro site</h1>
	<p>
		Esse é meu primeiro site. Fiz ele utilizando HTML, que é uma linguagem usada para criar websites. A sigla significa HyperText Markup Language (Linguagem de Marcação de Hipertexto). É a linguagem que todos os navegadores que utilizamos (como o Google Chrome ou o Internet Explorer) conseguem interpretar.
	</p>
</body>
```

### Formatação

Para dar destaque ou um significado diferenciado a algumas partes de um texto, podemos usar tags de formatação. Para ver como isso funciona, siga o próximo passo:

**→** No parágrafo que acabamos de adicionar, substitua as palavras `meu primeiro site` por `<strong>meu primeiro site</strong>`.

Além da tag `<strong>`, que deixa o texto em negrito, há também a tag `<em>`, que deixa o texto em itálico, a tag `<small>`, que diminui o texto e a tag `<big>`, que aumenta. Experimente usar essas outras tags em seu texto e veja o que acontece.

### Links

Links são a maneira de fazer com que os usuários naveguem para outras páginas através de um site. A tag para criar links é `<a>` e utilizamos o atributo `href` para especificar para qual página queremos levar o usuário.

**→** Adicione o seguinte link ao final do seu parágrafo (antes de fechar a tag `<p>`): `<a href="https://pt.wikipedia.org/wiki/HTML">Saiba mais sobre HTML...</a>`

**Dica:** Se você deseja que o link fique na linha de baixo, adicione a tag `<br/>` antes do link. Essa tag significa "quebra de linha".

### Divisões

A tag `<div>` permita que agrupemos elementos e possamos definir um "bloco" ali. Isso vai ser muito útil mais tarde, quando quisermos adicionar estilo (como uma cor de fundo) a um "bloco" de elementos.

**→** Adicione uma `<div>` ao redor do seu título e parágrafo. Seu código deve ficar como abaixo:

```html
<body>
	<div>
	  <h1>Como criei meu primeiro site</h1>
		<p>
			Esse é meu primeiro site. Fiz ele utilizando HTML, que é uma linguagem usada para criar websites. A sigla significa HyperText Markup Language (Linguagem de Marcação de Hipertexto). É a linguagem que todos os navegadores que utilizamos (como o Google Chrome ou o Internet Explorer) conseguem interpretar.<br/>
			<a href="https://pt.wikipedia.org/wiki/HTML">Saiba mais sobre HTML...</a>
		</p>
	</div>
</body>
```

### Lista

Para adicionar listas, usamos a tag `<ul>`. Para cada elemento da lista, usamos a tag `<li>`. Vamos criar um outro título e adicionar uma lista à página.

**→** Adicione o seguinte código depois de `</div>`:

```html
	<h2>O que eu já aprendi sobre HTML</h2>
	<ul>
	  <li>Estrutura básica</li>
	  <li>Títulos</li>
	  <li>Links</li>
	</ul>
```

Veja no navegador como ficou agora a página.

Também podemos adicionar *listas ordenadas* a uma página. Troque a tag `<ul></ul>` por `<ol></ol>` e veja qual é a mudança que acontece.

**Dica:** `ul` siginifica unordered list (lista desordenada), `ol` significa ordered list (lista ordenada) e `li` significa list item (item da lista).

### Imagem

Agora que já aprendemos sobre como adicionar texto em vários formatos, vamos aprender a adicionar imagens em uma página!
Primeiramente, precisamos salvar a imagem que queremos no computador.

**1 →** Crie uma pasta com o nome "imagens" dentro da mesma pasta em que está seu arquivo HTML.

**2 →** Com o navegador, acesse o site https://i.imgur.com/SZTerQc.png.

**3 →** Clique na imagem com o botão direito e selecione *Salvar imagem como…*.

**4 →** Selecione a pasta "imagens" que você criou e nomeie o arquivo como "imagem1".

Um elemento de imagem é formado por três partes:

* A tag `<img>`
* O atributo `src`, que é onde colocamos o "caminho" para a imagem que queremos
* O atributo `alt`, que provê uma descrição da imagem

Adicione o seguinte código em sua página, antes da tag `<div>`: `<img src="imagens/imagem1.png" alt="Capa do site">`.

## Finalizando

Com cada passo que completamos, o conteúdo final do arquivo *site.html* é o seguinte:

```html
<!DOCTYPE html>
<html>
	<head>
		<title>Meu primeiro site</title>
		<meta charset="utf-8">
	</head>
	<body>
    <img src="imagens/imagem1.png" alt="Capa do site">

  	<div>
  	  <h1>Como criei meu primeiro site</h1>
  		<p>
  			Esse é meu primeiro site. Fiz ele utilizando HTML, que é uma linguagem usada para criar websites. A sigla significa HyperText Markup Language (Linguagem de Marcação de Hipertexto). É a linguagem que todos os navegadores que utilizamos (como o Google Chrome ou o Internet Explorer) conseguem interpretar. <br/>
  			<a href="https://pt.wikipedia.org/wiki/HTML">Saiba mais sobre HTML...</a>
  		</p>
  	</div>

    <h2>O que eu já aprendi sobre HTML</h2>
  	<ul>
  	  <li>Estrutura básica</li>
  	  <li>Títulos</li>
  	  <li>Links</li>
  	</ul>

  </body>
</html>

```

E a página que o navegador mostra é como a imagem abaixo:

![Página mostrada pelo navegador](https://i.imgur.com/y0s87FW.png)

E assim acabamos de construir nossa primeira página HTML!
