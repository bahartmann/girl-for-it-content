Melhorias no conteúdo – Girl For It

Dia 1 - Scratch
[email] Dia 1 – Inserir na apostila a dinâmica do avião: incluir 1 monitora como integrantes de cada equipe/atividade (talvez seria uma maneira de integração)
[email] Dia 1 – precisamos pensar em uma dinâmica de quebra-gelo entre as meninas e as monitoras (considerando que serão profissionais da tecnologia conduzindo a atividade e geralmente são mais tímidos)

Dia 1 – incluir contextualização da Informática ( o que é TI? O que é programação?) antes de iniciar a introdução do histórico das mulheres na TI.

Dia 1 – A parte introdutória com o histórico das mulheres ficou um pouco distante da realidade delas. Precisamos trazer mais referências atuais.

Dia 1 – Na parte de introdução a algoritmos precisamos incluir um momento para elas exercitarem esse conceito, por ex. perguntar qual seria o algoritmo para tomar banho. Pensar em algum exercício mais simples para elas.

Inserir tempo para criação do tema do Blog

Dia 2 - HTML
Dia 2 –Aumentar HTML (de 45 min para 2h).
Dia 2 – seguimos o ritmo das meninas mais lentas (e isso esta ok). Só precisamos incluir mais conteúdo de código e depois parar para dar as explicações e atenção pontual. Assim o grupo mais adiantado pode visualizar o que estão fazendo e podemos incentivá-las a fazerem ajustes (qndo fizer sentido) naquilo que já construiram. Dessa forma não ficam ociosas.
incluir nota na agenda ref: Entrega resposta exercicios extra + correção (estimar tempo para correção) e Entrega folha com info de instituições e cursos
Iniciar a apresentação do CSS (Mentor explica e deixa meninas criarem sozinhas).

 Dia 3 – CSS (decoração do blog, tempo livre para criar, tempo para organizar a apresentação)
Aumentar o tempo do Bate papo para 40 minutos.
Apresentar/Introduzir o CSS (20 mins) dentro do tempo de Desenvolvimento do Blog.
Incluir como publicar o blog e alinhar as expectativas + como levar para casa o conteúdo (http://www.bitballoon.com/ - site para hospedar o blog das meninas gratuitamente, colocar o passo a passo);
Entrega formulário de feedback  (deixar tempo no final para isso)
Deixar um tempo para elas organizarem a apresentação.


Sites:
https://pastebin.com/LwgzN5aX - html
https://pastebin.com/zvEyzajs- CSS
Para imagem: https://picjumbo.com
Clicar sobre a imagem com o botão direto do mouse e colar no local da foto no HMTL.
O tamanho e formato da imagem que irá aparecer depende do que escolheu no CSS.
Para cores: http://www.flatuicolorpicker.com/category/green
Colocar o código da cor no CSS.


Apostila
Dia 2 - Adicionar hierarquia HTML (como no slide)
Adicionar “Glossário” para teclas de atalho
Dia 2 - Pg 24 falta fechar o meta (html)
Pg 42 e 43 – revisar links/referencias para que todas tenham a versão em português.
Adicionar na apostila do monitor o número das páginas correspondentes a página das participantes.
Dia 2 - Adicionar passos de como realizar a troca da cor de fundo no HTML
Dia 2 – ajuste da cor do codigo HTML para melhor visualização da projeção e impressão da apostila.
Dia 1 – há uma referência da personagem “princesa” perdida na parte do game
Dia 1 – Revisar o termo “a personagem”, melhor fazer referencia direta ao nome da personagem, por ex. “o rato Jerry” para não confundir
Dia 1 – ajuste página 12 ref o loop “SEMPRE”, no print do código esta ok.
Dia 2 - Revisão dos itens que não seriam necessários (ex. CSS com comandos não tão usados).
Algumas máquinas tiveram problema com a fonte escolhida. Precisamos adicionar a validação da fonte e configuração das máquinas no checklist pré-evento.

Especificações Técnicas – inserir na apostila
Sala:
Acesso a internet
Projetor
Preferencialmente em formato “tradicional” de sala de aula
Nos computadores:
Instalação do Software Free - Sublime Text 3 https://www.sublimetext.com/
Para rodar o Scratch é preciso:
(1) Adobe Flash Player released on or after June 15, 2016;
(2) web browser: one of the latest two versions of Chrome (Windows, ChromeOS, Mac or Linux), Firefox (Windows or Mac only), Safari (Mac only), Edge (Windows only), or Internet Explorer 11 (Windows only).
