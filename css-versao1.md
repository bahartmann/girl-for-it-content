# CSS

Quando estamos escrevendo HTML, cada tag representa um elemento específico. Para cada elemento, o navegador tem uma exibição diferente. Um título `<h1>`, por exemplo, fica em negrito e em uma fonte maior. Porém, para fazer sites com cores, tamanhos e espaçamentos diferenciados, essa estilização padrão não é suficiente. Para isso, precisamos customizar o estilo da página, e esse é exatamente o objetivo do CSS.

O CSS é uma linguagem para estilizar páginas na Internet. A sigla significa Cascading Style Sheets (folhas de estilo em cascata). Enquanto o HTML é responsável pela estruturação dos elementos de uma página na Internet, o CSS é responsável pela apresentação customizada desses elementos.

## Estrutura de um arquivo CSS

Um arquivo CSS é formado por várias regras. Uma regra tem o seguinte formato:

```css
h1 {
  color: blue;
}
```

* `h1` é o **seletor**, é o que indica em qual elemento será aplicada a regra. Nesse caso, estamos selecionando o título.
* `color` é a **propriedade**, é o que define que aspecto do elemento será mudado. Nesse caso, vamos mudar a *cor*.
* `blue` é o **valor** correspondente à propriedade. Nesse caso, vamos usar a cor *azul*.

Vamos aprender como CSS funciona na prática?

## Criando um arquivo CSS

Primeiro, vamos criar nossa folha de estilo.

**→** Na mesma pasta onde está seu arquivo HTML, crie um arquivo CSS e o nomeie como *estilo.css*.

No arquivo criado, vamos colocar as regras de estilo para nossa página HTML. Para a página reconhecer esse estilo, siga os passos:

**1 →** Abra seu arquivo HTML.
**2 →** Dentro da tag `<head>`, adicione a seguinte linha: `<link rel="stylesheet" href="estilo.css">`.

## Adicionando regras CSS

Agora que temos uma folha de estilo para nossa página, vamos criar algumas regras. A primeira delas será para mudar a fonte.

**→** Adicione o seguinte trecho ao seu arquivo *estilo.css*:

```css
body {
   font-family: Helvetica, Arial, sans-serif;
}
```

Você consegue identificar qual é o seletor nesta regra? E a propriedade? E o valor?

Observe também onde a regra que criamos foi aplicada e como isso se reflete na página. Quando selecionamos um elemento, a regra será aplicada a todos os elementos "aninhados" dentro dele.

Agora, vamos remover os "pontos" de cada item da lista que criamos.

**→** Adicione o seguinte trecho ao seu arquivo *estilo.css*:

```css
ul {
   list-style: none;
}
```

Quando estamos definindo o CSS, podemos adicionar várias propriedades em uma só regra. Tudo que fica entre as chaves `{}`, são propriedades. Veja abaixo como isso funciona.

**→** Para definir um estilo diferente para os links, adicione o seguinte:

```css
a {
  color: #37a2f7;
  text-decoration: none;
  border-bottom: 1px dotted #37a2f7;
}
```

O que cada propriedade e valor siginificam? Você pode adicionar uma de cada vez para ver o efeito de cada propriedade na página. Abaixo, uma explicação sobre cada propriedade:

### color

Essa é a propriedade para definir *cor*. Nesse caso, estamos definindo que a cor é um tipo de azul, representado pelo código `#37a2f7`. Para cada cor diferente, há um *código hexadecimal* diferente. Para algumas cores, podemos substituir o código pelo nome da cor. Veja algumas cores nomeadas abaixo:

![Lista de cores](https://i.imgur.com/xSCsC3t.png)

Para saber mais sobre cores em páginas HTML, acesse o endereço [http://html-color-codes.info/Codigos-de-Cores-HTML/](http://html-color-codes.info/Codigos-de-Cores-HTML/).

### text-decoration

A propriedade text-decoration (decoração de texto) permite que mudemos o estilo dos textos. Alguns valores que podem ser usados são: underline (sublinhado), overline (sobre-linha) and line-through (linha atravessada). Todos os links, por padrão, já vem com algumas decorações. Nesse caso, estamos usando *none* (nenhum) para remover essas decorações padrão.

### border-bottom

Essa propriedade define o estilo da *linha inferior* de um elemento. Todas as propriedades de borda podem ser definidas da seguinte forma: `border-bottom: espessura estilo cor`. No nosso caso, a espessura é 1 pixel, o estilo é pontilhado (*dotted*) e a cor é igual a cor da fonte.

Nesse ponto, seu arquivo CSS deve estar assim:

```css
body {
   font-family: Helvetica, Arial, sans-serif;
}

ul {
   list-style: none;
}

a {
   color: #37a2f7;
   text-decoration: none;
   border-bottom: 1px dotted #37a2f7;
}
```
