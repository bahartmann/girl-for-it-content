# Agenda Girl for IT

| Dia 1                                           |          |
|-------------------------------------------------|:--------:|
| Check-in + Apresentação do evento + Boas vindas | 30 min   |
| Histórico das Mulheres na TI                    | 15 min   |
| Quebra-gelo                                     | 30 min   |
| Coffee Break                                    | 15 min   |
| Linguagens de Programação                       | 30 min   |
| Scratch e Games                                 | 2 horas  |
| Encerramento                                    | 10 min   |
| **Duração Total**                               | **4h10** |

| Dia 2                                           |          |
|-------------------------------------------------|:--------:|
| Revisão conteúdo anterior                       | 10 min   |
| HTML                                            | 45 min   |
| CSS                                             | 45 min   |
| Coffee Break                                    | 10 min   |
| Criar Blog                                      | 2 horas  |
| Painel com Mulheres na TI                       | 40 min   |
| Encerramento                                    | 15 min   |
| **Duração Total**                               | **4h45** |
