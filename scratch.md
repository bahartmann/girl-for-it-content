# Construindo um jogo com Scratch

Esse é um tutorial para construir um jogo enquanto aprendemos a programar! Para programar esse jogo, vamos usar o Scratch. Scratch é uma linguagem de programação gráfica, o que significa que ao invés de usar código somente em texto (como a maioria das linguagens de programação), ela é visual. Os comandos que vamos passar para o computador são blocos coloridos que podemos arrastar e organizar como desejamos, montando nosso programa.

> **Orientação para quem está guiando:**
Navegar pelo site https://scratch.mit.edu/ para as meninas se familiarizem, enquanto elas somente assistem. Mostrar a página inicial. Entrar na página de projetos para iniciantes ("Starter Projects") e escolher um projeto da seção de games (Pong Starter, por exemplo). Jogar um pouco o jogo para mostrar o que a ferramenta é capaz de fazer e despertar o interesse das participantes. Não se esqueça de ir traduzindo as seções do site mencionadas para o português. Essa introdução ao Scratch não deve levar mais do que 5 minutos. Depois disso, seguir para o próximo passo, onde as próprias participantes vão realizar o cadastro e navegar pela ferramenta.

## Cadastro no site do Scratch

Agora, vamos fazer um cadastro no site do Scratch para que possamos salvar os projetos feitos. Seguiremos os seguintes passos:

> **Orientação para quem está guiando:**
Em cada passo a passo, utilize os números de cada item como uma ferramenta para ter certeza de que todas estão no mesmo ponto das orientações. Comece com o primeiro passo e só diga para fazerem o próximo quando todas tiverem feito o passo anterior, e assim sucessivamente.

**1 →** Acesse o site https://scratch.mit.edu/.

**2 →** Clique em "Join Scratch" ("Se juntar ao Scratch").

**3 →** Escolha um nome (sem espaços) e uma senha e preencha. Use uma senha que será lembrada! Clique em "Next" ("Próximo").

**4 →** Preencha com o mês e ano de nascimento, gênero e país. Clique em "Next".

**5 →** Preencha duas vezes seu e-mail. Se você não tiver e-mail, preencha com *sememail@bol.com*. Depois você poderá alterar essa informação. Clique em "Next".

**6 →** Pronto! Agora é só clicar em "OK Lets go!" ("OK, vamos lá!").

Clique em "Explore" ("Explorar") na barra de cima e veja os projetos que tem por lá!

## Vamos criar o nosso projeto!

Vamos aprender diversos conceitos de programação construindo um jogo!

> **Orientação para quem está guiando:**
Você pode ver o jogo pronto aqui: https://scratch.mit.edu/projects/171081991/. Esse é o resultado final que queremos o jogo. As meninas vão começar seu jogo a partir de outro projeto já existente. O projeto existente está nesse link: https://scratch.mit.edu/projects/171181948/.

No jogo que vamos construir, vamos ter uma personagem principal que tem como objetivo pegar um doce. Porém, no meio do caminho até o doce, há um morcego e um vilão que vão atrapalhar sua jornada.

**Curiosidade:** Quando estamos programando, chamamos a pessoa que vai usar o programa (ou seja, quem vai jogar o jogo) de usuária. Hoje, você não é a usuária, mas sim, a programadora do jogo!

Para começar, entre no site https://scratch.mit.edu/projects/171181948/#editor. Esse é um projeto do Scratch. A parte da esquerda é onde vamos ver nosso jogo em ação! É possível ver que alguns objetos já foram adicionados nessa parte. Cada um desses objetos é chamado de "sprite". Depois, vamos poder adicionar diversos comportamentos à esses objetos. Eles serão nossos personagens.

A parte da direita, é onde vamos adicionar comandos para o nosso jogo.


### Criando a personagem principal

Já temos alguns personagens no jogo, mas precisamos criar a personagem principal. Para isso, siga os passos abaixo:

**1 →** Clique no bonequinho ao lado do botão "New sprite" ("Novo objeto").

**2 →** Escolha uma das imagens para ser sua personagem. Você pode rolar a página para ver mais opções. Clique na imagem escolhida e depois no botão "OK".

Agora a personagem está no jogo! Mas como você pode ver, ela está muito maior que os outros objetos. Para corrigir isso e posicionar a personagem, siga os passos:

**3 →** Clique no botão "Shrink" ("Encolher") que está na barra bem no alto da tela.

**4 →** Clique na sua personagem até que fique em um tamanho parecido com os outros objetos. Quando estiver em um tamanho que você goste, clique em qualquer lugar do fundo lilás.

**5 →** Você pode mover os objetos pela tela arrastando eles. Vamos colocar a personagem no lado esquerdo do jogo. Coloque o ponteiro do mouse em cima da sua personagem, segure o botão do mouse e o mova enquanto continua segurando o botão. Largue o botão quando a personagem estiver onde você quer.


### Mexendo com comandos - os blocos

No meio da tela, estão os comandos que podem ser dados aos objetos. No Scratch, esses comandos são chamados de *blocos*. Clique no bloco "Move 10 steps" ("Ande 10 passos") e observe o que acontece com sua personagem quando você faz isso.

Há muitos tipos de blocos. Na parte de cima dos blocos, você  pode mudar o tipo dos blocos que estão sendo mostrados.

**1 →** Clique em "Looks" ("Aparência") para mostrar os blocos que mudam a aparência dos objetos.

**2 →** Clique no bloco "say Hello! for 2 secs" ("dizer Olá! por 2 segundos") e veja o que acontece.

**3 →** Observe o que acontece quando você clica em "think Hmm... for 2 secs" ("pensar Hmm... por 2 segundos").

**4 →** Clique também no bloco "hide" ("esconder"), depois no bloco "show" ("mostrar") e acompanhe o que cada um faz.

Agora vamos fazer a personagem falar "Oi!".

**5 →** Altere o bloco "say Hello! for 2 secs" para "say Olá! for 2 secs".

Na parte direita da tela está uma área vazia. É nesse lugar que vamos juntar os blocos para fazer comandos mais complicados. Essa junção de comandos que vai ser nosso programa!


### Personalizando o morcego

O morcego vai ficar atrapalhando nossa personagem em seu caminho para pegar o doce. Vamos fazer um programa para que o morcego fique se mexendo de cima para baixo.

**1 →** Clique no morcego.

**2 →** Pra começar, queremos que o morcego vá para cima. Clique no tipo de bloco "Motion" ("Movimento") e arraste o bloco "point in direction" ("apontar para direção") para a área do programa.

**3 →** Clique na flechinha para baixo e escolha "(0) up" ("0 graus - cima"). Depois, clique no bloco que você acabou de criar. Com isso, nós estamos apontamos a direção do morcego para cima, mas também giramos o morcego.

**4 →** Para manter a direção do morcego mas impedir ele de girar, arraste o bloco "set rotation style" ("definir estilo de rotação") para a área do programa. Clique na flecha para baixo e delecione "don't rotate" ("não girar"). Clique no bloco e veja o acontece. Pronto! Nosso morcego está na rotação certa novamente!

**5 →** Arraste um bloco "move 10 steps" ("andar 10 passos") para seu programa. Clique no bloco. E veja o que acontece.

Nosso morcego agora se movimenta um pouco, mas logo depois para. Vamos agora fazer com que o morcego continue se movendo.

**6 →** Clique no tipo de bloco "Control" ("Controle"). Arraste um bloco "forever" ("para sempre") para o programa. Esse bloco vai repetir um comando repetidas vezes até que o jogo termine.

**7 →** Arraste o bloco "move 10 steps" para dentro do bloco "forever". O que acontece quando você clica nele?

**8 →** O morcego vai continuar indo para cima até chegar no limite da tela. Tente arrastar o morcego novamente para o centro da tela. Agora tente clicar no botão vermelho de parar e obsrve o que acontece.

**9 →** Clique no tipo de bloco "Motion". Arraste um bloco "if on edge, bounce" ("se está na borda, saltar de volta") para dentro do bloco "forever". Clique no bloco e veja o que acontece.

**10 →** Para fazer com que o morcego ande um pouco mais devagar, mude o bloco "move 10 steps", substituindo o 10 por 5. Veja como ficou agora!

> **Orientação para quem está guiando:**
Nesse momento, é importante dar uma pausa nos próximos passos e ter a atenção das participantes. Diversos passos foram adicionados, mas o que eles realmente fazem ainda não está claro. É hora de você explicar como o jogo e trazer os conceitos de condicional e loop! Use os parágrafos abaixo como apoio para as explicações. Encorage as meninas a largarem computador e o tutorial por um instante para ouvirem.

Mas como todos esses comandos funcionam? Quando rodamos o bloco forever, acontece o seguinte:

* O morcego anda 5 passos.
* O programa verifica se o morcego está na borda. Se estiver na borda, ele volta a direção. Se não, não faz nada.
* Como esses comandos estão dentro do bloco "forever", eles vão continuar repetindo: anda, verifica se está na borda, muda ou não a direção, anda, verifica se está na borda, muda ou não a direção, anda...

Com isso aprendemos dois conceitos muito usados em programação: o conceito de **condição** e o conceito de **repetição**.

**Condição** é alguma coisa que depende de outra para acontecer. No caso do nosso jogo, a condição é: se o objeto chegar na borda, ele salta de volta. Se não, continua para o próximo passo. Então para o *salto de volta* acontecer, depende do objeto chegar na borda. Isso é utilizado em vários programas que usamos normalmente. Por exemplo: toda vez que você abre o site Facebook, o código por trás do Facebook verifica se você tem novas solicitações de amizade. Se você tiver uma solicitação, vai aparecer uma indicação no ícone de amigos. Se não tiver novas solicitações, nada aparece.

**Repetição** é quando algo acontece repetidamente enquanto o programa está rodando. No caso do nosso jogo, o morcego continua se movendo e verificando se está na borda repetidamente. Isso também é usado em outros casos, como no jogo Tetris, em que as peças continuam caindo infitamente enquanto o jogo está rodando.

**11 →** Clique no tipo de bloco "Events" ("Eventos") e arraste o bloco "when 🏳 clicked" ("quando 🏳 for clicada") para seu programa. Esse comando é utilizado para detectar o que fazer quando alguém clica na bandeira.

**12 →** Agora é hora de juntar os comandos que programamos para o morcego! Arraste os blocos pare que fiquem na seguinte ordem, todos juntos: "when 🏳 clicked", "set rotation style", "point in direction" e "forever" (com os outros comandos dentro). O seu programa deve se parecer com a imagem abaixo.

![Programa do morcego](http://i.imgur.com/TbraIRw.png)

**13 →** Pare seu programa clicando no botão vermelho e reinicie clicando na bandeira verde.

Estamos prontas para a próxima etapa!


## Dando vida à personagem principal

Agora vamos fazer um programa para sua personagem se mover.

**1 →** Na seção que diz "Sprites", clique na imagem da sua personagem.

**2 →** Clique no pequeno "i" no canto da imagem. Na parte que diz "rotation style" ("estilo de rotação"), selecione a opção **↔**. Então clique na flecha de voltar, que está no canto de cima. Isso vai fazer com que a imagem da personagem possa inverter de um lado pro outro, mas impede que ela gire em todos os sentidos.

Agora nós vamos escrever um programa que vai permitir usar o teclado pra mover nossa personagem.

**3 →** Clique no tipo de bloco "Events". Arraste para o programa um bloco "when ___ key pressed" ("quando pressionar tecla ___ ").

**4 →** Clique na flecha para baixo e mude de "space" ("espaço") para "right arrow" ("flecha direita").

O que deve acontecer com nossa personagem quando o evento "tecla para direita é presionada" acontece? Ela deve se mover para a direita. Vamos adicionar os comandos para isso acontecer!

**5 →** Vá para o tipo de bloco "Motion". Arraste um bloco "point in direction __ " para o programa, encaixando no bloco "when ___ key pressed". Clique na flechinha do bloco e escolha a opção "(90) right" (90 graus - direita). Precisamos desse comando para dizer ao programa para que direção nossa personagem vai se mover.

> **Orientação para quem está guiando:**
Nesse ponto é interessante explicar o porquê de a direção para a direita é 90. 90 é uma medida em graus, que indica o tamanho do ângulo para o qual a imagem vai girar. Relembrar que quando queríamos mover o morcego para cima usamos a direção (0) up, pois a contagem da circunferência começa em cima e vai indo para a direita. Para fazer essa explicação, é interessante usar um quadro da sala de aula ou usar a seguinte imagem: ![Circunferência](http://imgur.com/o5gl9ir).

**6 →** Arraste um bloco "move __ steps" e junte aos outros blocos.

Agora experimente pressionar a tecla para direita do teclado. O que acontece? E se você pressionar a tecla para esquerda? Nada acontece. Nós precisamos adicionar ao nosso programa comandos para mover a personagem também quando as teclas para cima, para esquerda e para baixo. Nos próximos passos, vamos chegar a um programa como o da imagem abaixo.

![Movimentos da personagem principal](http://i.imgur.com/sESUTRc.png)

**7 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para cima. Troque as opções para formar os comandos "when *up arrow* key pressed" e "point in direction *0*".

**8 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para baixo. Troque as opções para formar os comandos "when *down arrow* key pressed" e "point in direction *180*".

**9 →** Repita os passos **3**, **4**, **5** e **6** para a personagem andar para cima. Troque as opções para formar os comandos "when *left arrow* key pressed" e "point in direction *-90*".

Agora experimente usar as teclas de todas as direções e veja o que acontece!

## Vencendo e Perdendo

Nesse momento, nosso jogo permite que a personagem se mova, mas nada acontece se ela chegar no doce ou se o morcego alcançar ela. Vamos escrever um programa para lidar com isso!

**1 →** Com sua personagem ainda selecionada, vá para os blocos do tipo "Events". Arraste o bloco "when 🏳 clicked" para o programa.

**2 →** Vá para os blocos do tipo "Control". Arraste um bloco "forever" e cole no bloco anterior.

**3 →** Agora arraste um bloco "if __ then" ("se __ então") para dentro do bloco "forever".

**4 →** Vá para os blocos do tipo "Sensing" ("detectar"). Arraste um bloco "touching __ ?" ("encostando em __ ?") para o espaço do bloco "if __ then". Segure o bloco pela palavra "touching" para funcionar.

**5 →** Clique na flechinha do bloco "touching" e escolha a opção "Doce".

**6 →** Vá para o tipo de bloco "Looks". Arraste um bloco "say __ for __ secs" para dentro do bloco "if __ then". Mude de "Hello!" para "Você venceu!" e de 2 para 1 segundo.

**7 →** Para que o jogo termine, vá para o tipo de bloco "Control" e arraste o bloco "stop all" para dentro do "if __ then".

Agora é hora de testar! Clique na bandeira e mova a personagem até o doce. O que acontece?

Como você pôde ver, toda vez que clicamos na bandeira para começar o jogo, nosso programa fica **repetitivamente** verificando se a personagem está enconstando no doce. Se a nossa **condição** for verdade, a personagem vence. Estamos usando novamente os conceitos de repetição e condição!

Quando nossa personagem encosta no morcego ou no vilão, ela deve perder o jogo. Vamos fazer isso acontecer! Depois dos próximos passos, nossos comandos vão ser comos os da imagem abaixo.

![Programa da personagem principal](http://i.imgur.com/SmlkYf2.png)

**8 →** Para fazer o morcego ser letal, adicione os blocos "if __ then", "touching __ ", "say __ for __ secs" e "stop __ " dentro do bloco "forever". Troque as opções para "touching *Morcego*" e "say *Você perdeu!* for *1* sec".

**9 →** Para agora fazer o vilão ser letal, adicione novamente os blocos "if __ then", "touching __ " e "say __ for __ secs" dentro do bloco "forever". Troque as opções para "touching *Vilão*" e "say *Você perdeu!* for *1* sec".

Agora reposicione novamente a personagem, aperte na bandeira e veja o que acontece quando nossa personagem encosta em um dos objetos letais.

## Tornando o vilão mais perigoso

Por enquanto, o vilão não é uma ameaça muito grande. Vamos fazer ele perseguir a personagem principal e tornar o jogo mais divertido!

**1 →** Clique na imagem do vilão.
**2 →** Vá para os blocos do tipo "Events". Arraste o bloco "when 🏳 clicked" para o programa.
**3 →** Vá para os bloos do tipo "Control". Arraste um bloco "forever" para o programa, anexado ao bloco anterior.
**4 →** Vá para os blocos "Motion". Arraste um bloco "point towards __ " ("apontar em direção ao __ ") para dentro do bloco "forever". Clique na flechinha do bloco e escolha sua personagem.
**5 →** Arraste um bloco "move __ steps" e coloque dentro do bloco "forever".

Aperte na bandeira para testar o jogo. Você vai ver que o vilão alcança a personagem. Vamos mudar isso!

**6 →** Mude a quantidade de passos que o vilão anda de "move 10 steps" para "move 2 steps".

O programa do vilão deve se parecer com a imagem abaixo:

![Programa do vilão](http://i.imgur.com/T02ZaFi.png)

## Finalizando

Nosso jogo já está pronto para ser jogado. Isso significa que você contruiu seu primeiro programa! Parabéns!

Agora, é hora de customizar ele com o que aprendemos por enquanto e descobrir novas funcionalidades do Scratch. Tente incrementar o seu jogo. Abaixo, algumas suestões do que ainda pode ser feito. Use a imaginação!

* Tente adicionar um efeito de som quando a personagem alcança o bolo.
* Tente definir uma posição fixa para todos os objetos. Assim, não é preciso reposicionar cada um antes de jogar novamente.
* Tente adicionar mais inimigos ou obstáculos para que o jogo seja ainda mais desafiador.

Quando puder, explore mais o site do Scratch! Há diversos projetos muito interessantes por lá.
